# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Accessibility',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(verbose_name='Titulo', max_length=200, blank=None)),
                ('slug', models.SlugField(max_length=100)),
                ('min_time', models.TimeField(verbose_name='min time', blank=True, null=True)),
                ('max_time', models.TimeField(verbose_name='max time', blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('assigned', models.ForeignKey(verbose_name='asignado a', to=settings.AUTH_USER_MODEL, null=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='created_by')),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='users', verbose_name='shared with')),
            ],
            options={
                'verbose_name_plural': 'Calendars',
                'verbose_name': 'crear calendario',
                'permissions': (('view_calendars', 'Can see existing calendars'),),
            },
        ),
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('event_start', models.DateTimeField()),
                ('event_end', models.DateTimeField()),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('observation', models.TextField(blank=True, null=True)),
                ('is_cancelled', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('calendar', models.ForeignKey(to='fullcalendar.Calendar')),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='created_by_event')),
                ('member', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='accessibility',
            name='calendar',
            field=models.ForeignKey(to='fullcalendar.Calendar'),
        ),
        migrations.AddField(
            model_name='accessibility',
            name='group',
            field=models.ForeignKey(to='auth.Group'),
        ),
    ]
