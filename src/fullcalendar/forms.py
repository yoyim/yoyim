import datetime
from django import forms
from django.contrib.auth.models import User, Group
from django.forms import ModelChoiceField, ModelForm, ModelMultipleChoiceField
from accounts.functions import get_users_of_member_group
from fullcalendar.models import Calendar, Events
from gym import settings
from main.functions import add_class, add_class_time_picker
from django.utils.translation import ugettext as _

__author__ = 'jona'


class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        if obj.first_name:
            name = obj.first_name + " " + obj.last_name
        else:
            name = obj.username
        return name


class UserModelMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        if obj.first_name:
            name = obj.first_name + " " + obj.last_name
        else:
            name = obj.username
        return name


class CalendarModelForm(ModelForm):
    form_meta = {'title': _("Calendar")}

    group = Group.objects.filter(name=settings.MEMBER_GROUP)
    workers = User.objects.exclude(groups__in=group)

    title = forms.CharField(max_length=100)
    assigned = UserModelChoiceField(workers, empty_label="", label=_('assigned to'))
    users = UserModelMultipleChoiceField(workers, label=_('shared with'))

    class Meta:
        model = Calendar
        exclude = ("created_by", "slug")

    def __init__(self, *args, **kwargs):
        super(CalendarModelForm, self).__init__(*args, **kwargs)
        add_class(self, ['title', 'assigned', 'users', 'max_time', 'min_time'])
        add_class_time_picker(self, ['max_time', 'min_time'])


class EventsModelForm(ModelForm):
    form_met = {'title': _('Event')}

    members = get_users_of_member_group()

    title = forms.CharField(max_length=100)
    event_start = forms.DateTimeField(input_formats=['%d/%m/%Y %I:%M %p'])
    event_end = forms.DateTimeField(input_formats=['%d/%m/%Y %I:%M %p'])
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 2, 'cols': 15}), required=False)
    observation = forms.CharField(widget=forms.Textarea(attrs={'rows': 2, 'cols': 15}), required=False)
    member = UserModelChoiceField(members, empty_label="")

    class Meta:
        model = Events
        exclude = ("is_cancelled", "created_by", "calendar")

    def __init__(self, *args, **kwargs):
        super(EventsModelForm, self).__init__(*args, **kwargs)
        add_class(self, ['title', 'event_start', 'event_end', 'description', 'member', 'observation'])

