from django.utils.translation import ugettext as _
from django.forms import ModelForm

from main.functions import add_class, add_class_date_picker
from main.models import Membership
from store.models import Product


class MembershipForm(ModelForm):
    form_meta = {'title': _('Nueva Membresia'), }

    class Meta:
        model = Membership
        exclude = ("product",)

    def __init__(self, *args, **kwargs):
        super(MembershipForm, self).__init__(*args, **kwargs)
        add_class(self, ['duration_frequency', 'duration_type', 'limit_visits_per_day', 'is_active'])
        add_class_date_picker(self, ['date_end'])


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ('name', 'price',)

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        add_class(self, ['name', 'price'])
