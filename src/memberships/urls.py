from django.conf.urls import url, patterns

urlpatterns = patterns('memberships.views',
    url(r'^$', "home", name="home"),
    url(r'^memberships_json/', "memberships_json", name="memberships_json"),
    url(r'^new/', "new", name="new"),
    url(r'^edit/(?P<id>\d+)/$', 'edit', name='edit'),
    url(r'^delete/(?P<id>\d+)/$', 'delete', name='delete'),
)