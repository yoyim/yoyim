from django.conf.urls import include, url, patterns
from django.contrib import admin

urlpatterns = patterns('memberships.views',
    url(r'^$', "home", name="home"),
    url(r'^new/', "new", name="new"),
    url(r'^edit/(?P<id>\d+)/$', 'edit', name='edit'),
    url(r'^delete/(?P<id>\d+)/$', 'delete', name='delete'),
)