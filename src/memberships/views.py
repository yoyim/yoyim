import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from gym import settings
from main.functions import generate_tasks_when, date_handler

from main.models import Membership, DURATION_TYPES, ENTRIES_LIMIT_DAILY
from main.views import get_datatable_data
from memberships.forms import MembershipForm, ProductForm
from memberships.functions import get_membership_or_404, get_category_membership, get_code_product_membership


@login_required
@permission_required('main.view_memberships', raise_exception=True)
def home(request):
    title = _('memberships')

    columns_title = ['id', _('name'), _('duration frequency'), _('duration type'), _('is active'),
                     _('created at')]
    columns_data = ['id', 'product__name', 'duration_frequency', 'duration_type', 'is_active',
                    'created_at']

    columns_hidden = ['id']

    json_service = reverse('memberships:memberships_json')

    return render(request, "memberships/list.html", locals())


@csrf_exempt
@login_required()
def memberships_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = Membership.objects.values('id', 'product__name', 'duration_frequency', 'duration_type', 'is_active',
                                     'created_at', )

    if query == '_in_range_created_at':
        rows = rows.filter(created_at__range=(range_since, range_to))

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]
    #
    for row in rows:
        row['product__name'] = '<a href="' + reverse('memberships:edit', args=[row['id']]) + '">' + row['product__name'] + '</a>'
        i = row['duration_type']
        i = [item[1] for item in DURATION_TYPES if item[0] == i]
        row['duration_type'] = i[0]

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@permission_required('main.add_membership')
@login_required
def new(request):
    title = _('new membership')
    if request.POST:
        form = MembershipForm(request.POST)
        form_product = ProductForm(request.POST)
        if form_product.is_valid() and form.is_valid():
            product = form_product.save(commit=False)
            product.category = get_category_membership()
            product.code = get_code_product_membership()
            product.save()

            member = form.save(commit=False)
            member.product = product
            member.save()
            generate_tasks_when(request, 'on_member_created', member)
            return redirect(reverse("memberships:home"))
    else:
        form = MembershipForm()
        form_product = ProductForm()
    form.DURATION_TYPES = DURATION_TYPES
    form.ENTRIES_LIMIT_DAILY = ENTRIES_LIMIT_DAILY
    return render(request, "memberships/form.html", locals())


@login_required
def edit(request, id):
    title = _('edit membership')
    membership = get_membership_or_404(id)
    if request.POST:
        form = MembershipForm(request.POST, request.FILES, instance=membership)
        form_product = ProductForm(request.POST, request.FILES, instance=membership.product)
        if form_product.is_valid() and form.is_valid():
            form_product.save()
            form.save()
            return redirect(reverse('memberships:home'))
    else:
        form = MembershipForm(instance=membership)
        form_product = ProductForm(instance=membership.product)
    form.DURATION_TYPES = DURATION_TYPES
    return render(request, 'memberships/form.html', locals())


@login_required
def delete(request, id):
    membership = get_membership_or_404(id)
    membership.delete()
    messages.add_message(request, messages.SUCCESS, _('<strong>Success!</strong>panel has been deleted.'))
    return redirect(reverse('memberships:home'))
