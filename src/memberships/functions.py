from django.http import Http404

from gym import settings
from main.models import Membership
from store.models import CategoryProduct, Product

__author__ = 'bicho'


def get_membership_or_404(pk):
    try:
        return Membership.objects.get(pk=pk)
    except Membership.DoesNotExist:
        message = "There isn't an company associated with your account"
        raise Http404


def get_category_membership():
    try:
        return CategoryProduct.objects.get(name=settings.CATEGORY_MEMBERSHIP_NAME)
    except CategoryProduct.DoesNotExist:
        category_product = CategoryProduct()
        category_product.name = settings.CATEGORY_MEMBERSHIP_NAME
        category_product.is_root = True
        category_product.save()
        return category_product


def get_code_product_membership():
    try:
        products = Product.objects.filter(category__name=settings.CATEGORY_MEMBERSHIP_NAME).order_by('-code')[:1]
        product = products.first()
        if not product or product.code is None or product.code is '':
            return settings.MEMBERSHIP_CODE_PREFIX + "1".zfill(settings.LONG_NUMBER_CODE_MEMBERSHIP)
        else:
            code = product.code
            data = code.split(settings.MEMBERSHIP_CODE_PREFIX[-1])
            number = int(data[1]) + 1
            code = settings.MEMBERSHIP_CODE_PREFIX + str(number).zfill(settings.LONG_NUMBER_CODE_MEMBERSHIP)
            return code
    except Product.DoesNotExist:
        return settings.MEMBERSHIP_CODE_PREFIX + "1".zfill(settings.LONG_NUMBER_CODE_MEMBERSHIP)
