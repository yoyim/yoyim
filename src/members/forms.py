import datetime

from django.forms import ModelForm
from django.utils.translation import ugettext as _

from accounts.functions import get_users_of_seller_group
from main.functions import add_class_date_picker, add_class_edited, add_class_datetime_picker
from main.models import PersonMembership, Membership, FrozenPersonMembership


class PersonMembershipForm(ModelForm):
    form_meta = {'title': _('new person'), }

    class Meta:
        model = PersonMembership
        fields = ('membership', 'since', 'observation', 'created_by', 'created_at')

    def __init__(self, *args, **kwargs):
        super(PersonMembershipForm, self).__init__(*args, **kwargs)
        self.fields['since'].widget.format = '%d/%m/%Y'
        self.fields['since'].input_formats = ['%d/%m/%Y']
        self.fields['created_at'].widget.format = '%d/%m/%Y %H:%M'
        self.fields['created_at'].input_formats = ['%d/%m/%Y %H:%M']
        # self.fields['observation'].widget = Textarea(attrs={'placeholder': _('What is the person target with us?')})
        self.fields['observation'].widget.attrs['cols'] = 1
        self.fields['observation'].widget.attrs['rows'] = 1
        self.fields['membership'].queryset = Membership.objects.filter(is_active=True, is_promo=True,
                                                                       date_end__gte=datetime.date.today()) | Membership.objects.filter(
            is_promo=False, is_active=True)
        self.fields['created_by'].queryset = get_users_of_seller_group()
        add_class_edited(self, ['membership', 'since', 'observation', 'created_by'])
        add_class_date_picker(self, ['since'])
        add_class_datetime_picker(self, ['created_at'])


class PersonMembershipContractForm(ModelForm):
    class Meta:
        model = PersonMembership
        fields = ('contract_number',)


class FrozenPersonMembershipForm(ModelForm):
    form_meta = {'title': _('new frozen'), }

    class Meta:
        model = FrozenPersonMembership
        fields = ('since', 'to', 'number')

    def __init__(self, *args, **kwargs):
        super(FrozenPersonMembershipForm, self).__init__(*args, **kwargs)
        self.fields['since'].widget.format = '%d/%m/%Y'
        self.fields['since'].input_formats = ['%d/%m/%Y']
        self.fields['to'].widget.format = '%d/%m/%Y'
        self.fields['to'].input_formats = ['%d/%m/%Y']

        add_class_date_picker(self, ['since', 'to'])
        add_class_edited(self, ['number'])
