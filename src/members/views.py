from copy import deepcopy
import datetime
from decimal import Decimal
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.dispatch import receiver
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from forms_builder.forms.models import Field, FieldEntry

from forms_builder.forms.signals import form_valid

from accounts.forms import PersonForm, generate_username
from accounts.functions import get_members_by_pattern, add_user_to_member_group, get_team_group
from gym import settings
from main.functions import generate_tasks_when, add_time_to_date, date_handler
from main.logger import AdminLogger, compare
from main.models import Attendance, Exercise, Prospectus, PersonMembership, FrozenPersonMembership, DAYS
from main.views import get_datatable_data
from members.forms import PersonMembershipForm, FrozenPersonMembershipForm, PersonMembershipContractForm
from accounts.models import Person
from members.functions import get_voucher_type_for_membership, get_active_membership_of_person, get_prospectus, \
    get_contract
from store.functions import get_customer
from store.models import VoucherProduct, VoucherProductDetails, CompanyTypeVoucher


@permission_required('accounts.view_members')
def home(request):
    title = _('members list')

    columns_title = ['id', _('name'), _('surname'), _('DNI'), _('membership'), _('from'), _('to'),
                     _('associated by'), _('created at')]
    columns_data = ['id', 'person__name', 'person__surname', 'person__dni', 'membership__product__name', 'since', 'to',
                    'created_by__first_name', 'created_at']

    columns_hidden = ['id']

    json_service = reverse('members:members_json')

    return render(request, 'members/home_list.html', locals())


@csrf_exempt
@login_required()
def members_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = PersonMembership.objects.values('id', 'person__id', 'person__name', 'person__surname', 'person__dni',
                                           'membership__product__name', 'since', 'to', 'created_by__first_name',
                                           'created_at')

    if query == '_in_range_created_at':
        rows = rows.filter(created_at__range=(range_since, range_to))

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        row['person__name'] = '<a href="' + reverse('members:person_details', args=[row['person__id']]) + '">' + row[
            'person__name'] + '</a>'
        row['person__surname'] = '<a href="' + reverse('members:person_details', args=[row['person__id']]) + '">' + row[
            'person__surname'] + '</a>'

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@permission_required('accounts.view_members')
def prospectus(request):
    title = _('prospectus list')

    columns_title = ['id', _('name'), _('surname'), _('dni'), _('email'), _('cellphone'), _('associated by'),
                     _('created at')]
    columns_data = ['id', 'name', 'surname', 'dni', 'email', 'cellphone', 'created_by__first_name', 'created_at']

    columns_hidden = ['id']

    json_service = reverse('members:prospectus_json')

    return render(request, 'prospectus/list.html', locals())


@csrf_exempt
@login_required()
def prospectus_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = Person.objects.values('id', 'dni', 'name', 'surname', 'email', 'cellphone', 'created_by__first_name',
                                 'created_at').exclude(id__in=PersonMembership.objects.values('person_id'))

    if query == '_in_range_created_at':
        rows = rows.filter(created_at__range=(range_since, range_to))

    rows = rows.exclude(user__groups__name=get_team_group())

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        if row['dni']:
            row['dni'] = '<a href="' + reverse('members:person_details', args=[row['id']]) + '">' + row['dni'] + '</a>'
        row['name'] = '<a href="' + reverse('members:person_details', args=[row['id']]) + '">' + row['name'] + '</a>'
        row['surname'] = '<a href="' + reverse('members:person_details', args=[row['id']]) + '">' + row[
            'surname'] + '</a>'
        # row['sex'] = Person.SEX.__getitem__(int(row['sex']) - 1)[1]

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required()
def last_10_to_enter(request):
    members = []
    attendances = Attendance.objects.all().order_by('-created_at')[:10]
    for attendance in attendances:
        if not members.__contains__(attendance.person_membership.person.user):
            members.append(attendance.person_membership.person.user)
    return render(request, 'members/home.html', locals())


@permission_required('accounts.view_members')
def search(request):
    title = 'members'
    pattern = request.GET.get('pattern')
    if pattern:
        members = get_members_by_pattern(pattern)

    else:
        members = Attendance.objects.all().order_by('created_at').person_set.all()[:10]
    return render(request, 'members/home.html', locals())


@login_required
def new(request):
    title = _('new person')
    if request.POST:
        form = PersonForm(request.POST, request.FILES)
        if form.is_valid():
            person = form.save(commit=False)
            person.created_by = request.user

            name = person.name.replace(' ', '')
            user = User.objects.create_user(
                generate_username(name),
                person.email,
                name,
            )
            user.first_name = person.name
            user.last_name = person.surname
            user.save()

            add_user_to_member_group(user)

            person.user = user
            person.save()

            logger = AdminLogger(request)
            logger.log_addition(person)

            generate_tasks_when(request, settings.EVENT_ON_PERSON_CREATED, person)

            return redirect(reverse('members:add_prospectus', args={person.id}))
    else:
        form = PersonForm()
    return render(request, 'members/form.html', locals())


@login_required
@permission_required('accounts.change_person')
def edit(request, id):
    member = get_object_or_404(Person, pk=id)
    tmp = deepcopy(member)
    if request.POST:
        form = PersonForm(request.POST, request.FILES, instance=member)
        if form.is_valid():
            person = form.save(commit=False)
            person.save()

            old, new = compare(tmp, person)
            # TODO send this old or new to message

            logger = AdminLogger(request)
            logger.log_change(person, "")

            return redirect(reverse('members:person_details', args={person.id}))
    else:
        form = PersonForm(instance=member)

        person_membership = PersonMembership.objects.filter(person=member).first()
        if person_membership:
            person_membership_id = person_membership.id
            person_membership = get_object_or_404(PersonMembership, pk=person_membership_id)
            form_contract = get_contract(request.user)
            entries = []
            fields = Field.objects.filter(form=form_contract)
            field = fields.get(slug='person_membership_id')
            if field:
                entries_id = FieldEntry.objects.values('entry').filter(value=person_membership_id, field_id=field.id)
                if entries_id:
                    entries = FieldEntry.objects.filter(entry__in=entries_id)

    return render(request, 'members/form.html', locals())


@login_required
@permission_required('accounts.delete_person')
def delete(request, id):
    member = get_object_or_404(Person, pk=id)
    member.delete()
    messages.add_message(request, messages.SUCCESS, _('<strong>Success!</strong> person has been deleted.'))
    return redirect(reverse('members:home'))


@login_required
@permission_required('main.add_personmembership')
def add_membership(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    title = _('adding membership to') + ' ' + person.name
    if request.POST:
        form = PersonMembershipForm(request.POST)
        if form.is_valid():
            person_membership = form.save(commit=False)
            person_membership.person = person
            # person_membership.created_by = request.user
            membership = person_membership.membership
            person_membership.to = add_time_to_date(person_membership.since, membership.duration_frequency,
                                                    membership.duration_type)

            person_membership.to_real = person_membership.to
            person_membership.save()

            generate_tasks_when(request, settings.EVENT_ON_MEMBERSHIP_ADDED_TO_PERSON, person,
                                preventive_date=person_membership.to)
            return redirect(reverse('members:person_details', args={person.id}))
    else:
        form = PersonMembershipForm()
    memberships = PersonMembership.objects.filter(person=person).order_by('to')

    return render(request, 'members/form_membership.html', locals())


@login_required
@permission_required('main.change_personmembership')
def edit_membership(request, person_id, id):
    person = get_object_or_404(Person, pk=person_id)
    person_membership = get_object_or_404(PersonMembership, pk=id)
    title = _('editing membership') + ' ' + _('to') + ' ' + person.name
    if request.POST:
        form = PersonMembershipForm(request.POST, instance=person_membership)
        if form.is_valid():
            person_membership = form.save(commit=False)
            membership = person_membership.membership
            person_membership.to = add_time_to_date(person_membership.since, membership.duration_frequency,
                                                    membership.duration_type)
            person_membership.to_real = person_membership.to
            person_membership.save()
            return redirect(reverse('members:person_details', args={person.id}))
    else:
        form = PersonMembershipForm(instance=person_membership)

    return render(request, 'members/form_membership.html', locals())


@login_required
@permission_required('main.delete_personmembership')
def delete_membership(request, person_id, id):
    person = get_object_or_404(Person, pk=person_id)
    person_membership = get_object_or_404(PersonMembership, pk=id)
    person_membership.delete()

    messages.add_message(request, messages.SUCCESS, _('<strong>Success!</strong>membership has been deleted.'))

    return redirect(reverse('members:person_details', args={person.id}))


@login_required
def contract(request, person_membership_id):
    person_membership = get_object_or_404(PersonMembership, pk=person_membership_id)
    form = get_contract(request.user)
    entries = []
    fields = Field.objects.filter(form=form)
    field = fields.get(slug='person_membership_id')
    if field:
        entry_id = FieldEntry.objects.values('entry').filter(value=person_membership_id, field_id=field.id).last()
        if entry_id:
            entry_id = entry_id['entry']
            entries = FieldEntry.objects.filter(entry=entry_id)

    person = person_membership.person

    title = _('adding contract to ') + person_membership.person.name

    field_to_hide = 'person_id'
    default_value = person.id
    field_to_hide2 = 'person_membership_id'
    default_value2 = person_membership_id

    if not entries:
        entries = person.__dict__
        if person_membership.voucher_product:
            entries['voucher_number'] = person_membership.voucher_product.__str__()

    form_person = PersonForm(instance=person)
    form_person_membership = PersonMembershipContractForm(instance=person_membership)

    return render(request, 'members/form_contract.html', locals())


def add_prospectus(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    entries = []
    title = _('adding prospectus')
    field_to_hide = 'person_id'
    default_value = person_id

    form = get_prospectus(request.user)
    fields = Field.objects.filter(form=form)

    if not entries:
        entries = person.__dict__

    return render(request, 'prospectus/form.html', locals())


@receiver(form_valid)
def set_person(sender=None, form=None, entry=None, **kwargs):
    field = entry.form.fields.get(label="person_id")
    field_entry, _ = entry.fields.get_or_create(field_id=field.id)
    person = get_object_or_404(Person, pk=int(field_entry.value))

    if entry.form.title == settings.PROSPECTUS_FORM_NAME:
        request = sender

        close_sale = entry.form.fields.get(label=settings.CLOSE_SALE)
        field_entry, _ = entry.fields.get_or_create(field_id=close_sale.id)

        prospectus = Prospectus()
        prospectus.entry = entry
        prospectus.person = person
        prospectus.user = request.user

        if 'No' in field_entry.value:
            generate_tasks_when(request, settings.EVENT_ON_PROSPECTUS_ADDED_BUT_NOT_MEMBERSHIP, person)
        else:
            prospectus.close_sale = True

        prospectus.save()
    if entry.form.title == settings.CONTRACT_FORM_NAME:

        field = entry.form.fields.get(label="person_membership_id")
        field_entry, _ = entry.fields.get_or_create(field_id=field.id)
        person_membership = get_object_or_404(PersonMembership, pk=int(field_entry.value))

        person_form = PersonForm(sender.POST, instance=person)
        person_membership_form = PersonMembershipContractForm(sender.POST, instance=person_membership)

        if person_form.is_valid():
            person_form.save()
        else:
            pass

        if person_membership_form.is_valid():
            person_membership.save()
        else:
            pass


def form_sent(request, slug):
    return redirect(reverse('members:home'))


def person_details(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    exercises = Exercise.objects.filter(is_active=True)

    form = get_prospectus(request.user)
    fields = Field.objects.filter(form=form)
    field = fields.filter(slug='person_id')
    if field:
        entries_id = FieldEntry.objects.filter(value=person.id, field_id=field[0].id).values('entry')
        if entries_id:
            entries = FieldEntry.objects.filter(entry__in=entries_id)

    last_10_memberships = PersonMembership.objects.filter(person=person).order_by('-created_at')[:10]
    last_10_attendances = Attendance.objects.filter(person_membership__person=person).order_by('-created_at')[:10]

    return render(request, 'members/person_details.html', locals())


def add_attendance(request, person_id):
    person = get_object_or_404(Person, id=person_id)
    person_membership = get_active_membership_of_person(person)

    if person_membership:
        if person_membership.active:

            count = FrozenPersonMembership.objects.filter(person_membership=person_membership,
                                                          since__lte=datetime.date.today(),
                                                          to__gte=datetime.date.today()).count()

            from_time = person_membership.membership.from_time
            to_time = person_membership.membership.to_time
            now = datetime.datetime.now().time()
            print(to_time)
            print(now)
            print(from_time)

            if count > 0:
                messages.add_message(request, messages.ERROR,
                                     _(
                                         '<strong>Error!</strong> %s there is a freeze for this membership' % person.name))
            elif to_time < now or now < from_time:
                messages.add_message(request, messages.ERROR,
                                     _(
                                         '<strong>Error!</strong> %(name)s, can enter from %(from)s to %(to)s ') % {
                                         'name': person.name,
                                         'from': from_time, 'to': to_time})
            else:
                attendance = Attendance(person_membership=person_membership, created_by=request.user)
                attendance.save()

                days_to_end = (person_membership.to - datetime.date.today()).days
                if days_to_end > 7:
                    messages.add_message(request, messages.SUCCESS,
                                         _(
                                             '<strong>Success!</strong> Attendance for %s has been registered.' % person.name))
                else:
                    messages.add_message(request, messages.WARNING,
                                         _(
                                             '<strong>Warning!</strong> Attendance registered, %s days to expired.' % str(
                                                 days_to_end)))

        else:
            messages.add_message(request, messages.ERROR,
                                 _('<strong>Error!</strong> %s dosen''t have any membership activated' % person.name))
    else:
        messages.add_message(request, messages.ERROR,
                             _('<strong>Error!</strong> %s dosen''t have any registered membership.' % person.name))

    if request.GET.get('keep_url'):
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return redirect(reverse('members:person_details', args={person.id}))


@csrf_exempt
@login_required()
def add_exercise(request):
    if request.POST:
        exercise_id = request.POST.get('exercise_id')
        attendance_id = request.POST.get('attendance_id')

        attendance = get_object_or_404(Attendance, id=attendance_id)
        attendance.exercises.add(get_object_or_404(Exercise, id=exercise_id))
        attendance.save()

    response = {
        'success': 'true'
    }

    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required()
def edit_voucher(request, person_membership_id):
    person_membership = get_object_or_404(PersonMembership, pk=person_membership_id)
    if not person_membership.voucher_product:
        company_voucher = get_object_or_404(CompanyTypeVoucher, pk=1)
        voucher_product = VoucherProduct(
            my_company=company_voucher.company,
            company_vouchers=company_voucher,
            subtotal=person_membership.membership.product.price,
            total=person_membership.membership.product.price,
            total_cashed=person_membership.membership.product.price,
            customer=get_customer(person_membership.person.dni, person_membership.person.id, True),
            type_voucher_product=get_voucher_type_for_membership(),
            created_by=request.user,
            form_pay_cash_amount=person_membership.membership.product.price,
        )
        voucher_product.status = voucher_product.UNPAID

        voucher_product.save()

        voucher_product_details = VoucherProductDetails(
            voucher_product=voucher_product,
            product=person_membership.membership.product,
            quantity=1,
            unitary_price=Decimal(person_membership.membership.product.price),
            discount=Decimal(0),
            sub_total=person_membership.membership.product.price,
            total=Decimal(person_membership.membership.product.price)
        )
        voucher_product_details.save()

        person_membership.voucher_product = voucher_product
        # person_membership.active = True
        person_membership.save()

    return redirect(reverse('store:voucher_view', args={person_membership.voucher_product.id}))


@csrf_exempt
@login_required
def get_members(request):
    pattern = request.GET.get('pattern')

    if pattern:
        objects = Person.objects.values('user', 'name', 'surname').filter(
            Q(name__icontains=pattern) | Q(surname__icontains=pattern))
    else:
        objects = []
    s_json = json.dumps(list(objects))
    return HttpResponse(s_json, content_type='application/json')


def freeze_membership(request, person_id, person_membership_id):
    person = get_object_or_404(Person, pk=person_id)
    if request.POST:
        form = FrozenPersonMembershipForm(request.POST)
        if form.is_valid():
            person_membership = get_object_or_404(PersonMembership, pk=person_membership_id)

            frozen_person_membership = form.save(commit=False)
            frozen_person_membership.person_membership = person_membership
            frozen_person_membership.created_by = request.user
            frozen_person_membership.save()

            days = calculate_person_membership_to(person_membership)

            person_membership.to = add_time_to_date(person_membership.to_real, days, DAYS)
            person_membership.save()

            return redirect(reverse('members:person_details', args={person.id}))
    else:
        form = FrozenPersonMembershipForm()

    return render(request, 'members/form_freeze_membership.html', locals())


def calculate_person_membership_to(person_membership):
    days = 0
    frozen_person_memberships = FrozenPersonMembership.objects.filter(person_membership=person_membership)
    for frozen_person_membership in frozen_person_memberships:
        different = frozen_person_membership.to - frozen_person_membership.since
        days += different.days
    return days


def delete_freeze_membership(request, person_id, id):
    frozen_person_membership = get_object_or_404(FrozenPersonMembership, pk=id)
    person_membership = frozen_person_membership.person_membership
    frozen_person_membership.delete()

    days = calculate_person_membership_to(person_membership)
    if days == 0:
        person_membership.to = person_membership.to_real
    else:
        person_membership.to = add_time_to_date(person_membership.to_real, days, DAYS)
    person_membership.save()

    return redirect(reverse('members:person_details', args={person_id}))
