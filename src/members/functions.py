from datetime import datetime

from forms_builder.forms import fields
from forms_builder.forms.models import Form, Field

from gym import settings
from main.models import PersonMembership
from store.models import TypeVoucherProduct


def get_voucher_type_for_membership():
    try:
        type_voucher_product = TypeVoucherProduct.objects.get(name__iexact=settings.VOUCHER_TYPE_FOR_MEMBERSHIPS)
    except TypeVoucherProduct.DoesNotExist:
        type_voucher_product = TypeVoucherProduct(name=settings.VOUCHER_TYPE_FOR_MEMBERSHIPS)
        type_voucher_product.save()
    return type_voucher_product


def get_active_membership_of_person(person):
    return PersonMembership.objects.filter(person=person, to__gt=datetime.now).first()


def get_prospectus(user):
    published = Form.objects.published(for_user=user)
    try:
        form = published.get(slug=settings.PROSPECTUS_FORM_NAME)
    except Form.DoesNotExist:
        form = Form(
            title=settings.PROSPECTUS_FORM_NAME,
            slug=settings.PROSPECTUS_FORM_NAME,
            redirect_url='/members/details/$person_id/',
            send_email=False)
        form.save()

        person_id_field = Field(
            form=form,
            label='person_id',
            field_type=fields.HIDDEN)
        person_id_field.save()

        sold_field = Field(
            form=form,
            label=settings.CLOSE_SALE,
            field_type=fields.RADIO_MULTIPLE,
            choices=settings.CLOSE_SALE_CHOICES
        )
        sold_field.save()
    return form


def get_contract(user):
    published = Form.objects.published(for_user=user)
    try:
        form = published.get(slug=settings.CONTRACT_FORM_NAME)
    except Form.DoesNotExist:
        form = Form(
            title=settings.CONTRACT_FORM_NAME,
            slug=settings.CONTRACT_FORM_NAME,
            redirect_url='/members/details/$person_id/',
            send_email=False)
        form.save()

        voucher_number = Field(
            form=form,
            label='voucher_number',
            field_type=fields.TEXT)
        voucher_number.save()

        person_membership_id = Field(
            form=form,
            label='person_membership_id',
            field_type=fields.HIDDEN)
        person_membership_id.save()


        person_id_field = Field(
            form=form,
            label='person_id',
            field_type=fields.HIDDEN)
        person_id_field.save()

    return form
