from django.conf.urls import url, patterns

urlpatterns = patterns('members.views',
    url(r'^home/', "home", name="home"),
    url(r'^members_json/', "members_json", name="members_json"),
    url(r'^prospectus/', "prospectus", name="prospectus"),
    url(r'^prospectus_json/', "prospectus_json", name="prospectus_json"),
    url(r'^search/', "search", name="search"),
    url(r'^last-10-to-enter/', "last_10_to_enter", name="last_10_to_enter"),

    # url(r'^prospectus/', "prospectus", name="prospectus"),

     url(r'^new/', "new", name="new"),
     url(r'^edit/(?P<id>\d+)/$', 'edit', name='edit'),
     url(r'^delete/(?P<id>\d+)/$', 'delete', name='delete'),
     url(r'^add_membership/(?P<person_id>\d+)/$', 'add_membership', name='add_membership'),
     url(r'^edit_membership/(?P<person_id>\d+)/(?P<id>\d+)/$', 'edit_membership', name='edit_membership'),
     url(r'^freeze_membership/(?P<person_id>\d+)/(?P<person_membership_id>\d+)/$', 'freeze_membership', name='freeze_membership'),
     url(r'^delete_freeze_membership/(?P<person_id>\d+)/(?P<id>\d+)/$', 'delete_freeze_membership', name='delete_freeze_membership'),
     url(r'^delete_membership/(?P<person_id>\d+)/(?P<id>\d+)/$', 'delete_membership', name='delete_membership'),
     url(r'^add_prospectus/(?P<person_id>\d+)/$', 'add_prospectus', name='add_prospectus'),
     url(r'^contract/(?P<person_membership_id>\d+)/$', 'contract', name='contract'),
     url(r'^details/(?P<person_id>\d+)/$', 'person_details', name='person_details'),
     url(r'^add-attendance/(?P<person_id>\d+)/$', 'add_attendance', name='add_attendance'),
     url(r'^add-exercise/', 'add_exercise', name='add_exercise'),
     url(r'^edit-voucher/(?P<person_membership_id>\d+)/$', 'edit_voucher', name='edit_voucher'),
     url(r'^members-json/', "get_members", name="get_members"),

)
