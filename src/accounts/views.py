import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from accounts.forms import UserForm, PersonForm
from accounts.functions import get_user, get_member_group, get_seller_group, get_team_group
from accounts.models import Person
from gym import settings
from main.functions import date_handler
from main.views import get_datatable_data


def message(request, code):
    if code == 'password_reset':
        message = _(
            '<strong>We have sent you an email!</strong> Please, follow the instructions to reset your password.')
        # messages.add_message(request, messages.INFO, message)
        return render(request, 'accounts/echo.html', locals())
        # return redirect(reverse('accounts:password_reset'))

    if code == 'password_reset_confirm':
        messages.add_message(request, messages.INFO, _(
            '<strong>Success!</strong> You have changed your password.'))
        return redirect(reverse('accounts:login'))

    if code == 'password_change_done':
        messages.add_message(request, messages.INFO, _(
            '<strong>Success!</strong> You have changed your password.'))
        return redirect(reverse('accounts:password_change'))

    return redirect(reverse('accounts:profile_edit'))


@permission_required('accounts.view_members')
def _list(request):
    title = _('personal')

    columns_title = ['id', _('username'), _('name'), _('surname'), _('created at')]
    columns_data = ['id', 'username', 'first_name', 'last_name', 'date_joined']

    columns_hidden = ['id']

    json_service = reverse('accounts:accounts_json')

    return render(request, 'accounts/list.html', locals())


@csrf_exempt
@login_required()
def accounts_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = User.objects.values('id', 'username', 'first_name', 'last_name', 'date_joined').filter(
        groups__name=get_team_group())
    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        row['username'] = '<a href="' + reverse('accounts:edit', args=[row['id']]) + '">' + row['username'] + '</a>'
    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


def new(request):
    if request.POST:
        user_form = UserCreationForm(request.POST)
        person_form = PersonForm(request.POST)
        if user_form.is_valid() & person_form.is_valid():
            user = user_form.save()

            member_group = get_team_group()
            member_group.user_set.add(user)
            member_group.save()

            person = person_form.save(commit=False)
            person.user = user
            person.save()
            return redirect(reverse('accounts:list'))
    else:
        user_form = UserCreationForm()
        person_form = PersonForm()
    return render(request, 'accounts/form.html', locals())


@login_required
def edit(request, id):
    user = get_user(id)
    if request.POST:
        user_form = UserCreationForm(request.POST, request.FILES, instance=user)
        person_form = PersonForm(request.POST, request.FILES, instance=user.person)
        if user_form.is_valid() & person_form.is_valid():
            user = user_form.save()
            person = person_form.save()
            return redirect(reverse('accounts:list'))
    else:
        user_form = UserCreationForm(instance=user)
        person_form = PersonForm(instance=user.person)
    return render(request, 'accounts/form.html', locals())


@login_required
def delete(request, id):
    user_profile = get_user(id)
    user_profile.delete()
    messages.add_message(request, messages.SUCCESS, _('<strong>Success!</strong>panel has been deleted.'))
    return redirect(reverse('accounts:list'))
