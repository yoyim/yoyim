from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from accounts.forms import UserForm, PersonForm
from accounts.functions import get_user


def message(request, code):
    if code == 'password_reset':
        message = _(
            '<strong>We have sent you an email!</strong> Please, follow the instructions to reset your password.')
        # messages.add_message(request, messages.INFO, message)
        return render(request, 'accounts/echo.html', locals())
        # return redirect(reverse('accounts:password_reset'))

    if code == 'password_reset_confirm':
        messages.add_message(request, messages.INFO, _(
            '<strong>Success!</strong> You have changed your password.'))
        return redirect(reverse('accounts:login'))

    if code == 'password_change_done':
        messages.add_message(request, messages.INFO, _(
            '<strong>Success!</strong> You have changed your password.'))
        return redirect(reverse('accounts:password_change'))

    return redirect(reverse('accounts:profile_edit'))


def list(request):
    # users = UserProfile.objects.exclude(type=MEMBER)
    return render(request, 'accounts/list.html', locals())


def new(request):
    if request.POST:
        user_form = UserCreationForm(request.POST)
        user_profile_form = UserForm(request.POST)
        person_form = PersonForm(request.POST)
        if user_form.is_valid() & user_profile_form.is_valid() & person_form.is_valid():
            user = user_form.save()
            person = person_form.save()
            user_profile = user_profile_form.save(commit=False)
            user_profile.user = user
            user_profile.person = person
            user_profile.save()
            return redirect(reverse('accounts:list'))
    else:
        user_form = UserCreationForm()
        user_profile_form = UserForm()
        person_form = PersonForm()
    return render(request, 'accounts/form.html', locals())


@login_required
def edit(request, id):
    user_profile = get_user(id)
    if request.POST:
        user_form = UserCreationForm(request.POST, request.FILES, instance=user_profile.user)
        user_profile_form = UserForm(request.POST, request.FILES, instance=user_profile.type)
        person_form = PersonForm(request.POST, request.FILES, instance=user_profile.person)
        if user_form.is_valid() & user_profile_form.is_valid() & person_form.is_valid():
            user = user_form.save()
            person = person_form.save()
            user_profile = user_profile_form.save(commit=False)
            user_profile.user = user
            user_profile.person = person
            user_profile.save()
            return redirect(reverse('accounts:list'))
    else:
        user_form = UserCreationForm(instance=user_profile.user)
        user_profile_form = UserForm(instance=user_profile.type)
        person_form = PersonForm(instance=user_profile.person)
    return render(request, 'members/form.html', locals())


@login_required
def delete(request, id):
    user_profile = get_user(id)
    user_profile.delete()
    messages.add_message(request, messages.SUCCESS, _('<strong>Success!</strong>panel has been deleted.'))
    return redirect(reverse('accounts:list'))
