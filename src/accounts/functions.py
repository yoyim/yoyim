import re

from django.contrib.auth.models import Group, User
from django.db.models import Q
from django.shortcuts import get_object_or_404
from gym import settings

__author__ = 'bicho'


def get_user(pk):
    try:
        return User.objects.get(pk=pk)
    except User.DoesNotExist:
        raise 404


def get_member_group():
    try:
        return Group.objects.get(name=settings.MEMBER_GROUP)
    except Group.DoesNotExist:
        group = Group(name=settings.MEMBER_GROUP)
        group.save()
        return group


def get_seller_group():
    try:
        return Group.objects.get(name=settings.SALES_GROUP)
    except Group.DoesNotExist:
        group = Group(name=settings.SALES_GROUP)
        group.save()
        return group


def get_team_group():
    try:
        return Group.objects.get(name=settings.TEAM_GROUP)
    except Group.DoesNotExist:
        group = Group(name=settings.TEAM_GROUP)
        group.save()

        sellers = get_users_of_seller_group()
        for s in sellers:
            group.user_set.add(s)
        group.save()

        return group


def get_users_of_seller_group():
    return get_seller_group().user_set.all()


def add_user_to_member_group(user):
    member_group = get_member_group()
    member_group.user_set.add(user)
    member_group.save()


def get_members_by_pattern(pattern):
    entry_query = get_query(pattern, ['person__nick', 'person__name', 'person__surname', 'person__dni'])
    found_entries = get_member_group().user_set.filter(entry_query)
    get_member_group().user_set.filter(entry_query)
    return found_entries


def get_members_created_by(user):
    group = get_member_group()
    if group:
        return group.user_set.filter(person__created_by=user)


def get_users_of_member_group():
    return get_member_group().user_set.all().order_by('first_name')


def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:

        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']

    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.

    '''
    query = None  # Query to search for every search term
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None  # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query
