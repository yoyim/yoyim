from django.contrib.auth.models import User
from django.db import models
from django.db.models import Model
from django.utils.translation import ugettext as _


class Person(Model):
    Man = 1
    Woman = 2

    SEX = (
        (Man, _("Man")),
        (Woman, _("Woman"))
    )

    dni = models.CharField(_('document'), max_length=8, null=True, blank=True)
    nick = models.CharField(_('nick'), max_length=100, null=True, blank=True)
    name = models.CharField(_('name'), max_length=100, null=False, blank=None)
    surname = models.CharField(_('surname'), max_length=100, null=False, blank=None)
    email = models.EmailField(_('email'), max_length=100, null=True, blank=True)
    image = models.ImageField(_('photo'), upload_to='photos', default='resources/Unknown-person.gif', null=True,
                              blank=True)
    birthday = models.DateField(_('date of birth'), null=True, blank=True)
    address = models.CharField(_('address'), max_length=255, null=True, blank=True)
    cellphone = models.CharField(_('cellphone'), max_length=20, null=True, blank=True)
    phone = models.CharField(_('phone'), max_length=20, null=True, blank=True)
    sex = models.IntegerField(_('sex'), choices=SEX, default=Man)
    user = models.ForeignKey(User, null=True)
    is_parent = models.BooleanField(_('has children'), default=False)  # TODO remove this field

    created_by = models.ForeignKey(User, related_name='person_created_by', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        permissions = (
            ("view_members", "Can see people registered by user"),
            ("view_all_members", "Can see all existing members"),
            ("add_prospectus", "Can add prospectus"),
            ("view_summary_sales_simple", "Can see the simple sales summary"),
            ("view_summary_memberships_sold", "Can see the sold memberships summary"),
            ("view_summary_members", "Can see the members summary"),
            ("view_summary_attendances", "Can see the attendances summary"),
            ("view_summary_absences", "Can see the absences summary"),
            ("view_summary_memberships", "Can see memberships summary"),
            ("view_summary_team_performance", "Can see team performance summary"),
            ("view_summary_extra", "Can see extra summary"),
            ("view_summaries", "Can see summaries"),
        )

    def __str__(self):
        return self.name + ' ' + self.surname


User.person = property(lambda s: Person.objects.get_or_create(user=s)[0])

# Person.vouchers = property(lambda s: Voucher.objects.filter(person=s).order_by('-created_at')[:10])
