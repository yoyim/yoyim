# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('dni', models.CharField(verbose_name='DNI', blank=True, max_length=8, null=True)),
                ('nick', models.CharField(verbose_name='apodo', blank=True, max_length=100, null=True)),
                ('name', models.CharField(verbose_name='nombre', blank=None, max_length=100)),
                ('surname', models.CharField(verbose_name='apellidos', blank=None, max_length=100)),
                ('email', models.EmailField(verbose_name='correo', blank=True, max_length=100, null=True)),
                ('image', models.ImageField(upload_to='photos', verbose_name='foto', default='resources/Unknown-person.gif', blank=True, null=True)),
                ('birthday', models.DateField(verbose_name='fecha de nacimiento', blank=True, null=True)),
                ('address', models.CharField(verbose_name='dirección', blank=True, max_length=255, null=True)),
                ('cellphone', models.CharField(verbose_name='celular', blank=True, max_length=20, null=True)),
                ('phone', models.CharField(verbose_name='teléfono', blank=True, max_length=20, null=True)),
                ('sex', models.IntegerField(default=1, choices=[(1, 'Hombre'), (2, 'Mujer')], verbose_name='sexo')),
                ('is_parent', models.BooleanField(default=False, verbose_name='tiene hijos')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(related_name='person_created_by', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'permissions': (('view_members', 'Can see people registered by user'), ('view_all_members', 'Can see all existing members'), ('add_prospectus', 'Can add prospectus'), ('view_summary_sales_simple', 'Can see the simple sales summary'), ('view_summary_memberships_sold', 'Can see the sold memberships summary'), ('view_summary_members', 'Can see the members summary'), ('view_summary_attendances', 'Can see the attendances summary'), ('view_summary_absences', 'Can see the absences summary'), ('view_summary_memberships', 'Can see memberships summary'), ('view_summary_team_performance', 'Can see team performance summary'), ('view_summary_extra', 'Can see extra summary'), ('view_summaries', 'Can see summaries')),
            },
        ),
    ]
