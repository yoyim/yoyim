from django.contrib.auth.models import User
from django.forms import ModelForm, RadioSelect
from django.utils.translation import ugettext as _
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm, SetPasswordForm

from main.functions import add_class, add_class_date_picker, add_class_edited, add_attr_required
from main.models import Person
from main.widgets import AvatarWidgetNoLink


class PasswordChangeFormEdited(PasswordChangeForm):
    meta = {'title': _('Change password'), 'button': _('Save'), 'action': '',
            'form_class': 'col-lg-8 col-lg-offset-2', 'header_class': 'header-form-profile'}

    def __init__(self, *args, **kwargs):
        super(PasswordChangeFormEdited, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': self.fields['old_password'].label})
        self.fields['new_password1'].widget.attrs.update(
            {'class': 'form-control input_reset_password', 'placeholder': self.fields['new_password1'].label})
        self.fields['new_password2'].widget.attrs.update(
            {'class': 'form-control input_reset_password', 'placeholder': self.fields['new_password2'].label})


class PasswordResetFormEdited(PasswordResetForm):
    meta = {'title': _('Reset password'), 'button': _('Reset'), 'action': '',
            'form_class': 'col-lg-8 col-lg-offset-2', 'header_class': 'header-form-profile'}

    def __init__(self, *args, **kwargs):
        super(PasswordResetFormEdited, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control input_reset_password', 'placeholder': self.fields['email'].label})


class SetPasswordFormEdited(SetPasswordForm):
    meta = {'title': _('Reset password'), 'button': _('Save'), 'action': '',
            'form_class': 'col-lg-8 col-lg-offset-2', 'header_class': 'header-form-profile'}

    def __init__(self, *args, **kwargs):
        super(SetPasswordFormEdited, self).__init__(*args, **kwargs)
        self.fields['new_password1'].widget.attrs.update({'class': 'form-control form-first input_reset_password',
                                                          'placeholder': self.fields['new_password1'].label})
        self.fields['new_password2'].widget.attrs.update(
            {'class': 'form-control form-last input_reset_password', 'placeholder': self.fields['new_password2'].label})


def generate_username(base, correlative=''):
    username = base + str(correlative)
    try:
        User.objects.get(username=username)
        if correlative == '':
            correlative = 0
        correlative = int(correlative)
        correlative += 1
        return generate_username(username, correlative)
    except User.DoesNotExist:
        return username


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('username',)


class PersonForm(ModelForm):
    form_meta = {'title': _('new person'), }

    class Meta:
        model = Person
        fields = (
            'name', 'surname', 'dni', 'nick', 'email', 'sex', 'birthday', 'cellphone', 'address', 'image',)
        widgets = {
            'image': AvatarWidgetNoLink,
            'sex': RadioSelect(choices=Person.SEX),
        }

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        add_class(self,
                  ['dni', 'nick', 'name', 'surname', 'email', 'sex', 'birthday', 'cellphone', 'address', 'image', ])
        add_class_date_picker(self, ['birthday'])
        add_class_edited(self, ['image'])
        add_attr_required(self, ['name', 'surname'])
