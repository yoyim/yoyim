/**
 * -----------------------------------------------------------------------
 */
CREATE OR REPLACE FUNCTION sales_product_report()
RETURNS TABLE (
	id int,
	name varchar,
	today numeric,
	week numeric,
	month numeric
) AS
$BODY$
BEGIN
	return QUERY SELECT
	sp.id,
	sp.name,
	(SELECT
		SUM(vpd.total)
	FROM
		store_voucherproductdetails as vpd
	WHERE
		vpd.product_id = sp.id AND
		vpd.created_at BETWEEN date_trunc('day', current_timestamp) AND current_timestamp
	) as today,
	(SELECT
		SUM(vpd.total)
	FROM
		store_voucherproductdetails as vpd
	WHERE
		vpd.product_id = sp.id AND
		vpd.created_at BETWEEN
			(current_date - cast(EXTRACT(DOW FROM current_timestamp) as integer))
			AND
			(current_date + (6 - cast(EXTRACT(DOW FROM current_timestamp) as integer)))
	) as week,
	(SELECT
		SUM(vpd.total)
	FROM
		store_voucherproductdetails as vpd
	WHERE
		vpd.product_id = sp.id AND
		vpd.created_at BETWEEN
			date_trunc('MONTH', current_date)
			AND
			date_trunc('MONTH', current_date) + INTERVAL '1 MONTH - 1 DAY'
	) as month
FROM
	store_product as sp
ORDER BY sp.name;

END;
$BODY$
LANGUAGE plpgsql;


/**
 * -----------------------------------------------------------------------
 */
CREATE OR REPLACE FUNCTION voucher_x_company(date_start date, date_end date)
RETURNS TABLE(
	id integer,
	id_company integer,
	company_name varchar(100),
	company_ruc varchar(100),
	id_vouchertype integer,
	vouchertype_name varchar(100),
	current_total numeric,
	max_income numeric(11,2),
	range_date_start date,
	range_date_end date,
	have_tax boolean,
	percent numeric(6,2),
	have_series boolean,
	name_required boolean
)AS
$BODY$
BEGIN
	return QUERY SELECT
			sctv.id,
			sc.id as id_company,
			sc.name as company_name,
			sc.ruc as company_ruc,
			stvp.id as id_vouchertype,
			stvp.name as vouchertype_name,
			(SELECT
				SUM(svp.total_cashed)
			FROM
				store_voucherproduct as svp
			WHERE
				svp.company_vouchers_id = sctv.id AND
				NOT is_deleted AND
				is_input AND
				status = 1 AND
				date_payed
				BETWEEN
					date_start
					AND
					date_end
			) AS current_total,
			sctv.max_income,
			date_start as range_date_start,
			date_end as range_date_end,
			stvp.have_tax,
			stvp.percent,
			stvp.have_series,
			stvp.name_required
		FROM
			store_companytypevoucher as sctv,
			store_company as sc,
			store_typevoucherproduct as stvp
		WHERE
			sctv.company_id = sc.id AND
			sctv.type_voucher_id = stvp.id AND
			NOT sctv.is_deleted;

END;
$BODY$
LANGUAGE plpgsql;

