from django.forms import ModelForm

from qualification.models import Qualification


class QualificationForm(ModelForm):
    class Meta:
        model = Qualification
        fields = ('user', 'point')
