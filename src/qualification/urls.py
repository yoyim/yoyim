from django.conf.urls import url, patterns

urlpatterns = patterns(
    'qualification.views',
    url(r'^home/(?P<id>\d+)/$', 'home', name='home'),
    url(r'^personal_list/$', 'personal_list', name='personal_list'),
    url(r'^personal_list_json/$', 'personal_list_json', name='personal_list_json'),

    url(r'^personal_points_list/$', 'personal_points_list', name='personal_points_list'),
    url(r'^personal_points_list_json/$', 'personal_points_list_json', name='personal_points_list_json'),

    url(r'^personal_points_list_json/$', 'personal_points_list_json', name='personal_points_list_json'),
)
