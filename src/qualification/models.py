from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _


class Qualification(models.Model):
    bad = 1
    normal = 2
    good = 3

    CHOSEN_POINT = (
        (bad, _('bad')),
        (normal, _('normal')),
        (good, _('good'))
    )

    user = models.ForeignKey(User)
    point = models.IntegerField(choices=CHOSEN_POINT)
    created_at = models.DateTimeField(default=datetime.now())
    created_at_date = models.DateField(default=datetime.today())
