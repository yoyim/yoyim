# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Qualification',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('point', models.IntegerField(choices=[(1, 'bad'), (2, 'normal'), (3, 'good')])),
                ('created_at', models.DateTimeField(default=datetime.datetime(2017, 6, 25, 22, 57, 42, 102974))),
                ('created_at_date', models.DateField(default=datetime.datetime(2017, 6, 25, 22, 57, 42, 102973))),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
