from django.contrib import admin
from django.contrib.admin import ModelAdmin

from qualification.models import Qualification


class QualificationAdmin(ModelAdmin):
    list_display = ('user', 'point','created_at')


admin.site.register(Qualification, QualificationAdmin)
