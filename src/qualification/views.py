import json

from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

from accounts.functions import get_team_group
from accounts.models import Person
from gym import settings
from main.functions import date_handler
from main.views import get_datatable_data
from django.utils.translation import ugettext as _

from qualification.forms import QualificationForm


@permission_required('accounts.view_members')
def personal_list(request):
    title = _('staff qualification')

    columns_title = ['id', _('username'), _('name'), _('surname'), _('created at')]
    columns_data = ['id', 'username', 'first_name', 'last_name', 'date_joined']

    columns_hidden = ['id']

    json_service = reverse('qualification:personal_list_json')

    return render(request, 'qualification/list.html', locals())


@csrf_exempt
@login_required()
def personal_list_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)
    rows = User.objects.values('id', 'username', 'first_name', 'last_name', 'date_joined').filter(
        groups__name=get_team_group())
    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        row['username'] = '<a href="' + reverse('qualification:home', args=[row['id']]) + '">' + row[
            'username'] + '</a>'
    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


def home(request, id):
    user = get_object_or_404(User, pk=id)
    person = user.person
    if request.POST:
        form = QualificationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _(
                '<strong>Success!</strong>, Thanks for your help, we are working to offer the best of us to you'))
        else:
            messages.add_message(request, messages.ERROR, _('<strong>Error!</strong>, Your puntuation wasn\'t save'))
    return render(request, 'qualification/home.html', locals())


def personal_points_list(request):
    title = _('staff qualification')

    columns_title = ['id', _('username'), _('name'), _('surname'), _('created at')]
    columns_data = ['id', 'username', 'first_name', 'last_name', 'date_joined']

    columns_hidden = ['id']

    json_service = reverse('qualification:personal_points_list_json')

    return render(request, 'qualification/list.html', locals())


def personal_points_list_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)
    rows = User.objects.values('id', 'username', 'first_name', 'last_name', 'date_joined').filter(
        groups__name=get_team_group())
    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        row['username'] = '<a href="' + reverse('qualification:home', args=[row['id']]) + '">' + row[
            'username'] + '</a>'
    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')

