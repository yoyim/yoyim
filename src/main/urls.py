from django.conf.urls import url, patterns

urlpatterns = patterns('main.views',
    url(r'^$', "home", name="home"),
    url(r'^panel/$', 'dashboard', name='dashboard'),
    url(r'^attendances/$', 'attendances', name='attendances'),
    url(r'^report-person-added/$', 'report_person_added', name='report_person_added'),
    url(r'^report-performance/$', 'report_performance', name='report_performance'),
    url(r'^report-members/$', 'report_members', name='report_members'),
    url(r'^report-members-json/$', 'report_members_json', name='report_members_json'),
    url(r'^report_memberships/$', 'report_memberships', name='report_memberships'),
    url(r'^report-memberships-json/$', 'report_memberships_json', name='report_memberships_json'),
    url(r'^report-memberships-sold/$', 'report_memberships_sold', name='report_memberships_sold'),
    url(r'^report-memberships-sold-json/$', 'report_memberships_sold_json', name='report_memberships_sold_json'),

    url(r'^report-memberships-sold-by-seller/$', 'report_memberships_sold_by_seller', name='report_memberships_sold_by_seller'),
    url(r'^report-memberships-sold-by-seller-json/$', 'report_memberships_sold_by_seller_json', name='report_memberships_sold_by_seller_json'),

    url(r'^report_attendees/$', 'report_attendees', name='report_attendees'),
    url(r'^report-attendees-json/$', 'report_attendees_json', name='report_attendees_json'),
    url(r'^report-absentees/$', 'report_absentees', name='report_absentees'),
    url(r'^report-absentees-json/$', 'report_absentees_json', name='report_absentees_json'),
    url(r'^report-sales/$', 'report_sales', name='report_sales'),
    url(r'^report-sales-json/$', 'report_sales_json', name='report_sales_json'),
    url(r'^report-sales-graphic/$', 'report_sales_graphic', name='report_sales_graphic'),
    url(r'^install/$', 'install', name='install'),
)
