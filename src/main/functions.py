import datetime
from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.utils.encoding import force_str

from gym import settings
from main.models import Event, UserTask, Task
from main.models import DAYS, WEEKS, MONTHS, YEARS, VISITS

__author__ = 'bicho'


def add_class(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control',
                # 'placeholder' : form.fields[field].label
            }
        )


def add_class_money(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control is_money',
                # 'placeholder' : form.fields[field].label
            }
        )


def add_class_edited(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control edited',
            }
        )


def add_attr_required(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "required": 'required',
            }
        )


def add_class_date_picker(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control date-picker',
                # 'placeholder': 'dd/mm/yyyy'
            }
        )


def add_class_datetime_picker(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control datetime-picker',
                # 'placeholder': 'dd/mm/yyyy'
            }
        )


def add_class_time_picker(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control timepicker timepicker-default',
                'placeholder': '00:00:00'
            }
        )


def add_class_image_upload(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'image-upload default',
            }
        )


def date_handler(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime(settings.DATETIME_FORMAT)
    if isinstance(obj, datetime.date):
        return obj.strftime(settings.DATE_FORMAT)
    if isinstance(obj, Decimal):
        return str(obj)
    return obj


def generate_breadcrumb_relations(links, index, parameters=None):
    link = links[index]
    parent = index
    result = []

    def data(item, i, params=None):
        if item:
            if item['params']:
                r = reverse(item['route'], args=params[i]['params'])
            else:
                r = reverse(item['route']) if item['route'] else '#'
            n = item['name'] if item['name'] else params[i]['name']
            p = item['parent']
        else:
            n = None
            r = None
            p = None
        return n, r, p

    while link:
        name, route, parent = data(item=link, i=parent, params=parameters)
        result.append((name, route))
        if parent:
            link = links[parent]
        else:
            break

    result.reverse()

    return result


# def calculate_date_to(since, duration_type, to=None):
#     if not to:
#         to = datetime.datetime.now()
#
#     if duration_type == DAYS:
#         to = to + datetime.timedelta(days=since)
#     if duration_type == WEEKS:
#         to = to + datetime.timedelta(weeks=since)
#     if duration_type == MONTHS:
#         to = add_months(to, since)
#     if duration_type == YEARS:
#         to = add_months(to, since * 12)
#     if duration_type == VISITS:
#         # TODO count the visits
#         pass
#     return to


# def substract_from_date(to, time, duration_type):
#     if not to:
#         to = datetime.datetime.now()
#     if duration_type == DAYS:
#         to = to - datetime.timedelta(days=time)
#     if duration_type == WEEKS:
#         to = to - datetime.timedelta(weeks=time)
#     if duration_type == MONTHS:
#         to = add_months(to, time)
#     if duration_type == YEARS:
#         to = add_months(to, time * 12)
#     if duration_type == VISITS:
#         # TODO count the visits
#         pass
#     return to


def add_time_to_date(date, time, duration_type):
    if duration_type == DAYS:
        date = date + datetime.timedelta(days=time)
    if duration_type == WEEKS:
        date = date + datetime.timedelta(weeks=time)
    if duration_type == MONTHS:
        date = monthdelta(date, time)
    if duration_type == YEARS:
        date = monthdelta(date, time * 12)
    if duration_type == VISITS:
        # TODO count the visits
        pass
    return date


def monthdelta(date, delta):
    m, y = (date.month + delta) % 12, date.year + ((date.month) + delta - 1) // 12
    if not m: m = 12
    d = min(date.day, [31,
                       29 if y % 4 == 0 and not y % 400 == 0 else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1])
    return date.replace(day=d, month=m, year=y)


# def add_months(sourcedate, months):
#     month = sourcedate.month - 1 + months
#     year = int(sourcedate.year + month / 12)
#     month = month % 12 + 1
#     day = min(sourcedate.day, calendar.monthrange(year, month)[1])
#     return datetime.date(year, month, day)


def get_event(event_key):
    try:
        event = Event.objects.get(key=event_key)
    except Event.DoesNotExist:
        value = event_key.replace('_', ' ')
        event = Event(key=event_key, value=value)
        event.save()
    return event


def generate_tasks_when(request, event_key, object, preventive_date=None):
    event = get_event(event_key)
    tasks = Task.objects.filter(event=event)
    for task in tasks:
        user_task = UserTask()
        user_task.user = request.user
        user_task.task = task
        user_task.content_type = ContentType.objects.get_for_model(object)
        user_task.object_id = object.id
        user_task.object_repr = force_str(object)
        if task.is_preventive and preventive_date:
            task.duration_frequency *= -1
            period = add_time_to_date(preventive_date, task.duration_frequency, task.duration_type)
        else:
            period = add_time_to_date(datetime.datetime.now(), task.duration_frequency, task.duration_type)
        user_task.date = period
        user_task.done = task.default_done

        user_task.save()
