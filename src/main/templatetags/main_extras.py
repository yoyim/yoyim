import datetime

from django import template
from forms_builder.forms import fields

register = template.Library()


@register.filter
def coma_to_point(val):
    return str(val).replace(',', '.')


@register.filter
def first_word(word):
    s = word.split(' ')
    return s[0]


@register.filter
def add_placeholder(field):
    field.field.widget.attrs.update({"placeholder": field.label})
    return field


@register.filter
def primary_group(user):
    groups = user.groups.all()
    if groups:
        return groups[0]
    return ''


@register.filter
def to_date(value):
    return datetime.datetime.strptime(value, "%Y-%m-%d").date()


@register.filter
def to_js(self, hour_in_time):
    str_count = []
    for month in self[hour_in_time]:
        str_count.append(int(month['count']))
    s = str(str_count)
    return s.replace('[', '').replace(']', '')


@register.filter
def get_class_name(d):
    return d.__class__.__name__


@register.filter
def equals(self, n):
    if int(self) == n:
        return True
    return False


@register.filter
def has_class(self, class_name):
    if 'class' in self.field.widget.attrs:
        if class_name in self.field.widget.attrs['class']:
            return True
    return False


@register.filter
def is_birthday(date):
    today = datetime.date.today()
    if date:

        if date.month == today.month and date.day == today.day:
            return True
    return False


@register.filter
def years_old(born):
    today = datetime.date.today()
    try:
        birthday = born.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birthday = born.replace(year=today.year, month=born.month + 1, day=1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year


@register.filter
def get_value(field):
    if field.value():
        return field.value()
    if field.data:
        return field.data
    return None


@register.filter
def get_choice_value(choice):
    # if isinstance(choice,tuple):
    #     return choice[0][0]
    if choice[0]:
        return choice[0]
    return None


@register.filter
def get_value_from_items(field, items):
    if isinstance(items, dict):
        if field.slug in items:
            return items[field.slug]
    else:
        for item in items:
            if field.id == item.field_id:
                if field.field_type == fields.DATE:
                    if item.value:
                        date = datetime.datetime.strptime(item.value, "%Y-%m-%d").date()
                        return date.strftime('%d/%m/%Y')
                return item.value
    return ''


@register.filter
def get_value_from_items_in_date_format(field, items):
    if isinstance(items, dict):
        if field.slug in items:
            return items[field.slug]
    else:
        for item in items:
            if field.id == item.field_id:
                if item.value:
                    try:
                        strftime = datetime.datetime.strptime(item.value, '%Y-%m-%d').date()
                        return strftime.strftime('%d/%m/%Y')
                    except:
                        pass
    return ''
