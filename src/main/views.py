import datetime
from functools import reduce
import json
import operator
import xlwt

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import connection
from django.db.models import Count, Max
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.text import slugify
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

from accounts.functions import get_users_of_seller_group, get_member_group, get_seller_group
from gym import settings
from main.functions import date_handler, get_event
from main.models import Attendance, Prospectus, PersonMembership, Person, Membership, MONTHS, YEARS
from members.functions import get_prospectus, get_contract
from store.models import TypeVoucherProduct


def home(request):
    hide_nav = True
    hide_footer = True
    title = 'AtixPlus'
    home_background = settings.HOME_BACKGROUND
    return render(request, "main/home.html", locals())


@login_required
def dashboard(request):
    title = "Panel de control"

    today = datetime.date.today()
    today_min = datetime.datetime.combine(today, datetime.time.min)
    today_max = datetime.datetime.combine(today, datetime.time.max)

    person_count = Person.objects.all().count()
    person_count_today = Person.objects.filter(created_at__range=(today_min, today_max)).count()

    prospectus = Prospectus.objects.filter(created_at__range=(today_min, today_max))
    prospectus_today_count = prospectus.count()
    prospectus_today_count_close_sale = prospectus.filter(close_sale=True).count()

    start_week = today - datetime.timedelta(today.weekday())
    end_week = start_week + datetime.timedelta(7)
    prospectus = Prospectus.objects.filter(created_at__range=(start_week, end_week))
    prospectus_week_count = prospectus.count()
    prospectus_week_count_close_sale = prospectus.filter(close_sale=True).count()

    prospectus = Prospectus.objects.filter(created_at__month=today.month)
    prospectus_month_count = prospectus.count()
    prospectus_month_count_close_sale = prospectus.filter(close_sale=True).count()

    person_memberships = PersonMembership.objects.values('membership__id', 'membership__product__name').filter(
        active=True).annotate(Count('membership'))

    for person_membership in person_memberships:
        id_ = person_membership['membership__id']
        person_membership['today_count'] = PersonMembership.objects.filter(membership__id=id_, created_at__range=(
            today_min, today_max)).count()
        person_membership['week_count'] = PersonMembership.objects.filter(membership__id=id_, created_at__range=(
            start_week, end_week)).count()
        person_membership['month_count'] = PersonMembership.objects.filter(membership__id=id_,
                                                                           created_at__month=today.month).count()

    attendances_count = Attendance.objects.all().count()
    attendances_today_count = Attendance.objects.filter(created_at__range=(today_min, today_max)).count()
    attendances_week_count = Attendance.objects.filter(created_at__range=(start_week, end_week)).count()
    attendances_month_count = Attendance.objects.filter(created_at__month=today.month).count()

    membership_spent_count = PersonMembership.objects.all().count()
    membership_spent_count_today = PersonMembership.objects.filter(created_at__range=(today_min, today_max)).count()
    return render(request, "main/dashboard.html", locals())


def custom_403(request):
    return render(request, "main/403.html")


def date_hour(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).strftime("%x H")


@login_required
def attendances(request):
    from django.utils.timezone import activate
    activate(settings.TIME_ZONE)
    # http://test2.triait.com/question/Hourly-grouping-of-rows-using-Django/

    labels = []
    series = []

    truncate_date = connection.ops.date_trunc_sql('month', 'created_at')
    qs = Attendance.objects.extra({'month': truncate_date})
    report = qs.values('month').annotate(Count('pk')).order_by('month')

    for r in report:
        date = datetime.datetime.strptime(r['month'], "%Y-%m-%d").date()
        labels.append(str(date.month) + '-' + str(date.year))

    hours_in_time = {}
    for x in range(24):
        # if x < 10:
        #     hours_in_time['0' + str(x)] = []
        # else:
        #     hours_in_time[str(x)] = []
        # hours_in_time[str(x)] = []
        hours_in_time[x] = []

    for hour_in_time in hours_in_time:
        series.append(hour_in_time)
        for r in report:
            year_month = str(r['month']).replace('-01', '', 1)  # 2014-09
            # if len(year_month) > 7:
            #     year_month = year_month.

            data = {'year_month': year_month, 'count': '0'}
            hours_in_time[hour_in_time].append(data)

    hours_in_month = Attendance.objects.extra(
        {'year': 'strftime("%%Y", created_at)', 'month': 'strftime("%%m", created_at)',
         'hour': 'strftime("%%H", created_at)'}, where=["created_at != %s"], params=["null"]) \
        .order_by('year', 'month', 'hour').values('year', 'month', 'hour').annotate(
        count=Count('id'))

    for t in hours_in_month:
        current_hour = int(t['hour'])
        current_month = str(t['month'])
        current_year = str(t['year'])
        current_count = str(t['count'])

        year_month_current = current_year + '-' + current_month
        data = {'year_month': year_month_current, 'count': current_count,}

        for idx, item in enumerate(hours_in_time[current_hour]):
            if year_month_current == item['year_month']:
                hours_in_time[current_hour][idx] = data
                break

        print(current_year + ' - ' + current_month + ' - ' + str(current_hour) + 'h. - ' + current_count)

    return render(request, "main/attendances_tmp2.html", locals())


@login_required
@permission_required('main.report_added_people_by_users')
def report_person_added(request):
    title = _('prospectus created')
    truncate_date = connection.ops.date_trunc_sql('day', 'created_at')
    qs = Person.objects.extra({'month': truncate_date})
    report = qs.values('created_by__first_name', 'month').order_by('-created_at').annotate(
        cregister=Count('created_by'), cmonth=Count('created_at'))

    data = {}
    keys = {'date'}

    for r in report:
        date = r['month']
        name = r['created_by__first_name']
        if name == '':
            name = "Unnamed"
        mount = r['cregister']
        data.setdefault(date, {})['date'] = date
        data.setdefault(date, {})[name] = mount
        keys.add(name)

    objts = []

    for i in sorted(data):
        objts.append(data[i])

    return render(request, 'main/report_person_added.html', locals())


@login_required
@permission_required('accounts.view_summary_members')
def report_members(request):
    title = _('members report')
    columns_title = ['person_id', _('DNI'), _('name'), _('surname'), _('email'), _('birthday'), _('sex'),
                     _('cellphone'),
                     _('created'), _('by'), _('membership'), _('end'), 'total_memberships']
    columns_data = ['person__id', 'person__dni', 'person__name', 'person__surname', 'person__email', 'person__birthday',
                    'person__sex', 'person__cellphone', 'person__created_at', 'person__created_by__first_name',
                    'membership_name', 'to',
                    'count_ids']

    columns_non_orderable = ['membership_name', 'to', 'count_ids']
    columns_hidden = ['person__id']

    show_range_fields = True
    show_timepicker = False
    filter_to_show_range_fields = ['_in_range_birthday', '_with_membership_expired_in_range_dates']
    sellers = get_users_of_seller_group()

    filter_options = [
        ('_all', _('with out specification')),
        ('_in_range_birthday', _('with birthday in range')),
        ('_with_membership_activated', _('with membership actived')),
        ('_without_membership_activated', _('without membership actived')),
        ('_with_membership_to_expire_in_next_7_days', _('with membership to expire in next 7 days')),
        ('_with_membership_expired_in_last_7_days', _('with membership to expired in last 7 days')),
        ('_with_membership_expired_in_range_dates', _('with membership expired in range dates')),
    ]
    json_service = reverse('main:report_members_json')
    return render(request, 'main/layout_report_by_user.html', locals())


@csrf_exempt
@login_required()
def report_members_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    people = PersonMembership.objects.values('person__id', 'person__dni', 'person__name', 'person__surname',
                                             'person__email',
                                             'person__birthday', 'person__sex', 'person__cellphone',
                                             'person__created_at', 'person__created_by__first_name').annotate(
        count_ids=Count('person'))

    by_user = request.POST.get('byUser')
    if by_user:
        people = people.filter(person__created_by__id=by_user)

    today = datetime.datetime.today()
    if query == '_in_range_birthday':

        # Build the list of month/day tuples.
        monthdays = [(range_since.month, range_since.day)]
        while range_since <= range_to:
            monthdays.append((range_since.month, range_since.day))
            range_since += datetime.timedelta(days=1)

        # Tranform each into queryset keyword args.
        monthdays = (dict(zip(("person__birthday__month", "person__birthday__day"), t))
                     for t in monthdays)

        query = reduce(operator.or_, (Q(**d) for d in monthdays))
        people = people.filter(query)
    elif query == '_with_membership_activated':
        people = people.filter(to__gte=today)
    elif query == '_without_membership_activated':
        people = people.filter(to__lte=today)
    elif query == '_with_membership_to_expire_in_next_7_days':
        end_date = today + datetime.timedelta(days=7)
        people = people.filter(to__range=(today, end_date))
    elif query == '_with_membership_expired_in_last_7_days':
        since_date = today - datetime.timedelta(days=7)
        people = people.filter(to__range=(since_date, today))
    elif query == '_with_membership_expired_in_range_dates':
        people = people.filter(to__range=(range_since, range_to))

    people = people.order_by(field_order_by)

    iTotalDisplayRecords = people.count()

    if end:
        people = people[start:end]

    for row in people:
        last_membership = \
            PersonMembership.objects.values('membership__product__name', 'to').filter(
                person_id=row['person__id']).order_by(
                '-to')[0]
        row['membership_name'] = last_membership['membership__product__name']
        row['to'] = last_membership['to']
        row['person__dni'] = '<a href="' + reverse('members:person_details', args=[row['person__id']]) + '">' + row[
            'person__dni'] + '</a>'

    result = {'data': list(people), 'iTotalRecords': people.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required
@permission_required('accounts.view_summary_memberships')
def report_memberships(request):
    title = _('memberships')
    columns_title = [_('name'), _('duration'), _('periodo'), _('cost'), _('created at'), _('members')]
    columns_data = ['product__name', 'duration_frequency', 'duration_type', 'product__price', 'created_at', 'members']

    filter_options = [
        ('_all', _('all')),
    ]
    json_service = reverse('main:report_memberships_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def report_memberships_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)
    rows = Membership.objects.annotate(members=Count('personmembership__id')).values('product__name',
                                                                                     'duration_frequency',
                                                                                     'duration_type', 'product__price',
                                                                                     'created_at', 'members')

    today = datetime.datetime.today()

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        if int(row['duration_type']) == MONTHS:
            row['duration_type'] = _('months')
        if int(row['duration_type']) == YEARS:
            row['duration_type'] = _('year')

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    dumps = json.dumps(result, default=date_handler, check_circular=True)
    return HttpResponse(dumps, content_type='application/json')


@login_required
@permission_required('accounts.view_summary_attendances')
def report_attendees(request):
    title = _('attendances report')
    columns_title = [_('name'), _('surname'), _('membership'), _('last attendees')]
    columns_data = ['person_membership__person__name',
                    'person_membership__person__surname',
                    'person_membership__membership__product__name',
                    'created_at', ]

    # use _in_range_[field] to allow select a range filter for same field
    show_range_fields = True
    show_timepicker = True
    print('filterShowRange filterShowRange filterShowRange')
    filter_to_show_range_fields = ['_in_range_with_hour_created_at', ]
    filter_options = [
        ('_all', _('all')),
        ('_today', _('today')),
        ('_yesterday', _('yesterday')),
        ('_last_7_days', _('last 7 days')),
        ('_in_range_with_hour_created_at', _('in range')),
    ]
    json_service = reverse('main:report_attendees_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def report_attendees_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request)

    rows = Attendance.objects.values('person_membership',
                                     'person_membership__person__name',
                                     'person_membership__person__surname',
                                     'person_membership__membership__product__name',
                                     'created_at')

    if query == '_in_range_with_hour_created_at':
        print(range_since)
        print(range_to)

        rows = rows.filter(created_at__range=(range_since, range_to))
    elif query == '_today':
        today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
        today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        rows = rows.filter(created_at__range=(today_min, today_max))
    elif query == '_yesterday':
        day = datetime.date.today() - datetime.timedelta(1)
        day_min = datetime.datetime.combine(day, datetime.time.min)
        day_max = datetime.datetime.combine(day, datetime.time.max)
        rows = rows.filter(created_at__range=(day_min, day_max))
    elif query == '_last_7_days':
        day = datetime.date.today() - datetime.timedelta(7)
        day_min = datetime.datetime.combine(day, datetime.time.min)
        rows = rows.filter(created_at__range=(day_min, datetime.datetime.today()))

    iTotalDisplayRecords = rows.count()

    rows = rows.order_by(field_order_by)

    if end:
        rows = rows[start:end]

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required
@permission_required('accounts.view_summary_absences')
def report_absentees(request):
    title = _('absentees')
    columns_title = [_('name'), _('surname'), _('last attendees')]
    columns_data = ['person_membership__person__name',
                    'person_membership__person__surname',
                    'max_created', ]

    # use _in_range_[field] to allow select a range filter for same field
    show_range_fields = True
    show_timepicker = True
    filter_to_show_range_fields = ['_in_range_created_at']
    filter_options = [
        ('_all', _('all')),
    ]
    json_service = reverse('main:report_absentees_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def report_absentees_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request)

    today = datetime.datetime.now()
    date_limit = today - datetime.timedelta(days=7)
    rows = Attendance.objects \
        .values('person_membership',
                'person_membership__person__name',
                'person_membership__person__surname') \
        .annotate(Count('person_membership'), max_created=Max('created_at')) \
        .filter(person_membership__active=True,
                person_membership__to__gt=datetime.datetime.now(),
                person_membership__since__lt=datetime.datetime.now(),
                max_created__lt=date_limit)

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required
@permission_required('accounts.view_summary_sales_simple')
def report_sales(request):
    title = _('sales')
    columns_title = [_('name'), _('day'), _('week'), _('month')]
    columns_data = ['name',
                    'day',
                    'week',
                    'month']

    # use _in_range_[field] to allow select a range filter for same field
    show_range_fields = True
    sum_colums = [1, 2, 3]
    total_column = min(sum_colums)
    number_colums = len(columns_title) - total_column
    range_cols = range(0, number_colums)
    filter_to_show_range_fields = ['_in_range_created_at']
    filter_options = [
        ('_all', _('all')),
    ]
    json_service = reverse('main:report_sales_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def report_sales_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request)

    today = datetime.datetime.now()
    date_limit = today - datetime.timedelta(days=7)
    rows = get_sales_report()

    iTotalDisplayRecords = len(rows)

    if end:
        rows = rows[start:end]

    result = {'data': rows, 'iTotalRecords': len(rows), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


def get_sales_report():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM sales_product_report()")
    row = cursor.fetchall()
    answ = []
    for r in row:
        data = {
            'id': r[0],
            'name': r[1],
            'day': r[2],
            'week': r[3],
            'month': r[4]
        }
        answ.append(data)
    return answ


def get_datatable_data(request, date_format=settings.DATETIME_FORMAT):
    field_order_by = '-id'
    query = '_all'
    range_to = datetime.date.today()
    range_since = datetime.date.today()
    if not request.POST:
        start = 0
        end = 10
    else:
        start = int(request.POST['start'])
        length = int(request.POST['length'])
        order = int(request.POST.get('order[0][column]'))
        query = request.POST.get('query')

        if query and '_in_range_with_hour' in query:
            range_since = datetime.datetime.strptime(request.POST.get('range_since'), date_format)
            range_to = datetime.datetime.strptime(request.POST.get('range_to'), date_format)
            # range_to = range_to + datetime.timedelta(minutes=1)
        elif query and '_in_range' in query:
            range_since = datetime.datetime.strptime(request.POST.get('range_since'), date_format)
            range_to = datetime.datetime.strptime(request.POST.get('range_to'), date_format)
            range_to = range_to + datetime.timedelta(hours=23) + datetime.timedelta(minutes=59)

        if order > 0:
            field_order_by = request.POST.get('columns[' + str(order) + '][data]')
            order_dir = request.POST.get('order[0][dir]')
            if order_dir != 'asc':
                field_order_by = '-' + field_order_by
        else:
            field_order_by = request.POST.get('columns[0][data]')
            field_order_by = '-' + field_order_by
        end = None
        if length > 0:
            end = start + length
    return end, field_order_by, query, start, range_since, range_to


def get_export_query(request, date_format=settings.DATETIME_FORMAT):
    range_to = datetime.date.today()
    range_since = datetime.date.today()

    title = request.GET.get('title')
    order = request.GET.get('order')
    query = request.GET.get('query')

    if query and '_in_range_with_hour' in query:
        range_since = datetime.datetime.strptime(request.GET.get('range_since'), date_format)
        range_to = datetime.datetime.strptime(request.GET.get('range_to'), date_format)

    elif query and '_in_range' in query:
        range_since = datetime.datetime.strptime(request.GET.get('range_since'), date_format)
        range_to = datetime.datetime.strptime(request.GET.get('range_to'), date_format)
        range_to = range_to + datetime.timedelta(hours=23) + datetime.timedelta(minutes=59)

    return title, query, range_since, range_to, order


@login_required
@permission_required('accounts.view_summary_memberships_sold')
def report_memberships_sold(request):
    title = _('memberships sold')
    columns_title = ['id', _('membership'), _('created at'), _('since'), _('to'), _('sold by'), _('name'),
                     _('surname'), 'DNI', _('total'), _('with cash'), _('with card')]
    columns_data = ['id', 'membership__product__name', 'created_at', 'since', 'to', 'created_by__username',
                    'person__name', 'person__surname', 'person__dni', 'membership__product__price',
                    'voucher_product__form_pay_cash_amount', 'voucher_product__form_pay_card_amount']

    # columns_non_orderable = ['membership_name', 'to', 'count_ids']
    columns_hidden = ['id']
    sum_colums = [9]
    total_column = min(sum_colums)
    number_colums = len(columns_title) - total_column
    range_cols = range(0, number_colums)
    show_range_fields = True
    show_timepicker = False
    filter_to_show_range_fields = ['_in_range_created_at']

    filter_options = [
        ('_all', _('all')),
        ('_in_range_created_at', _('with sales in range')),
        # ('_with_membership_activated', _('with membership actived')),
        # ('_without_membership_activated', _('without membership actived')),
        # ('_with_membership_to_expire_in_next_7_days', _('with membership to expire in next 7 days')),
        # ('_with_membership_expired_in_last_7_days', _('with membership to expired in last 7 days')),
    ]
    json_service = reverse('main:report_memberships_sold_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def report_memberships_sold_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = PersonMembership.objects.values('id', 'membership__product__name', 'created_at', 'since', 'to',
                                           'created_by__username', 'person__name', 'person__surname', 'person__dni',
                                           'membership__product__price', 'voucher_product__form_pay_cash_amount',
                                           'voucher_product__form_pay_card_amount')

    if query == '_in_range_created_at':
        rows = rows.filter(created_at__range=(range_since, range_to))

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required
@permission_required('accounts.view_summary_team_performance')
def report_performance(request):
    start_at = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
    end_at = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

    if request.POST:
        filter = request.POST.get('filter')
        if filter == 'yesterday':
            day = datetime.date.today() - datetime.timedelta(1)
            start_at = datetime.datetime.combine(day, datetime.time.min)
            end_at = datetime.datetime.combine(start_at.date(), datetime.time.max)
        elif filter == 'last_5_days':
            day = datetime.date.today() - datetime.timedelta(7)
            start_at = datetime.datetime.combine(day, datetime.time.min)
        elif filter == 'the_month':
            start_at = datetime.date(start_at.year, start_at.month, 1)

    sellers = get_users_of_seller_group()

    memberships = PersonMembership.objects.values('membership', 'membership__product__name',
                                                  'membership__product__price').filter(
        created_at__range=(start_at, end_at)).annotate(Count('membership'))

    for seller in sellers:
        seller.person_memberships = PersonMembership.objects.values('membership').filter(created_by=seller,
                                                                                         created_at__range=(
                                                                                             start_at,
                                                                                             end_at)).annotate(
            Count('membership'))
        seller.count_memberships_sold = PersonMembership.objects.values('membership').filter(created_by=seller,
                                                                                             created_at__range=(
                                                                                                 start_at,
                                                                                                 end_at)).count()
        seller.count_people_registered = Person.objects.values('id').filter(created_by=seller, created_at__range=(
            start_at, end_at)).count()
        # for membership in memberships:
        #     if person_memberships['membership'] == membership['membership']:
        #         seller.person_memberships.append({
        #             'name': membership['membership__product__name'],
        #             'count':
        #         })

    return render(request, 'main/report_performance.html', locals())


@login_required
@permission_required('accounts.view_summary_sales_graphic')
def report_sales_graphic(request):
    # TODO fix with the next line sql request, try use a postgres function
    # PersonMembership.objects.extra({'published': "date(created_at)"}).values('published','membership').annotate(membership_count=Count('membership'))
    state = None
    if request.POST:
        filter = request.POST.get('filter')
        if filter == 'active':
            state = True
        if filter == 'inactive':
            state = False
    else:
        state = True

    if state is not None:
        dates = PersonMembership.objects.extra({'published': "date(main_personmembership.created_at)"}).filter(
            membership__is_active=state).values('published').annotate(
            Count('id')).order_by('published')
    else:
        dates = PersonMembership.objects.extra({'published': "date(main_personmembership.created_at)"}).values(
            'published').annotate(
            Count('id')).order_by('published')

    for date in dates:
        published_ = date['published']
        del date['published']
        date['date'] = published_.strftime("%d/%m/%y")
        if state is not None:
            counter = PersonMembership.objects.filter(membership__is_active=state, created_at__range=(
                datetime.datetime.combine(published_, datetime.time.min),
                datetime.datetime.combine(published_, datetime.time.max))). \
                values('membership__product__name').annotate(membership_count=Count('membership'))
        else:
            counter = PersonMembership.objects.filter(created_at__range=(
                datetime.datetime.combine(published_, datetime.time.min),
                datetime.datetime.combine(published_, datetime.time.max))). \
                values('membership__product__name').annotate(membership_count=Count('membership'))

        for count in counter:
            date[count['membership__product__name']] = count['membership_count']

    if state is not None:
        memberships = Membership.objects.filter(is_active=state)
    else:
        memberships = Membership.objects.all()

    return render(request, 'main/report_sales_graphic.html', locals())


@login_required
@permission_required('accounts.view_summary_memberships_sold')
def report_memberships_sold_by_seller(request):
    title = _('memberships sold')
    columns_title = ['id', _('created at'), 'DNI', _('surname'), _('name'), _('voucher'), _('contract'),
                     _('membership'), _('since'), _('to'), _('sold by'), _('total'), _('with cash'), _('with card')
                     ]
    columns_data = ['id', 'created_at', 'person__dni', 'person__surname', 'person__name', 'voucher_product__number',
                    'contract_number', 'membership__product__name', 'since', 'to', 'created_by__username',
                    'voucher_product__total', 'voucher_product__form_pay_cash_amount',
                    'voucher_product__form_pay_card_amount']

    # columns_non_orderable = ['membership_name', 'to', 'count_ids']
    columns_hidden = ['id']
    sum_colums = [11, 12, 13]
    total_column = min(sum_colums)
    number_colums = len(columns_title) - total_column
    range_cols = range(0, number_colums)
    show_range_fields = True
    show_timepicker = False
    filter_to_show_range_fields = ['_in_range_created_at']

    filter_options = [
        ('_all', _('all time')),
        ('_in_range_created_at', _('in period :')),
    ]

    sellers = get_users_of_seller_group()
    json_service = reverse('main:report_memberships_sold_by_seller_json')
    export_service = reverse('main:report_memberships_sold_by_seller_export')
    return render(request, 'main/layout_report_by_user.html', locals())


@csrf_exempt
@login_required()
def report_memberships_sold_by_seller_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = PersonMembership.objects.values('id', 'person__id', 'person__dni', 'person__surname', 'person__name',
                                           'voucher_product__number',
                                           'contract_number',
                                           'membership__product__name', 'created_at', 'since', 'to',
                                           'created_by__username',
                                           'voucher_product__total', 'voucher_product__form_pay_cash_amount',
                                           'voucher_product__form_pay_card_amount')

    by_user = request.POST.get('byUser')
    if by_user:
        rows = rows.filter(created_by__id=by_user)

    if query == '_in_range_created_at':
        print(range_since)
        print(range_to)
        rows = rows.filter(created_at__range=(range_since, range_to))

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        row['person__dni'] = '<a href="' + reverse('members:person_details', args=[row['person__id']]) + '">' + row[
            'person__dni'] + '</a>'

        row['person__surname'] = '<a href="' + reverse('members:person_details', args=[row['person__id']]) + '">' + row[
            'person__surname'] + '</a>'

    result = {'data': list(rows), 'iTotalRecords': rows.count(), 'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


def report_memberships_sold_by_seller_export(request):
    title, query, range_since, range_to, order_by = get_export_query(request, date_format=settings.DATE_FORMAT)
    title = slugify(title)
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="' + title + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(title)

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['id', _('created at'), 'DNI', _('surname'), _('name'), _('voucher'), _('contract'),
               _('membership'), _('since'), _('to'), _('sold by'), _('total'), _('with cash'), _('with card'),
               ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # rows = User.objects.all().values_list('username', 'first_name', 'last_name', 'email')
    rows = PersonMembership.objects.values_list('id', 'created_at', 'person__dni', 'person__surname', 'person__name',
                                                'voucher_product__number', 'contract_number',
                                                'membership__product__name', 'since', 'to', 'created_by__username',
                                                'voucher_product__total', 'voucher_product__form_pay_cash_amount',
                                                'voucher_product__form_pay_card_amount')

    by_user = request.GET.get('byUser')
    if by_user:
        rows = rows.filter(created_by__id=by_user)

    if query == '_in_range_created_at':
        print(range_since)
        print(range_to)
        rows = rows.filter(created_at__range=(range_since, range_to))

    rows = rows.order_by(order_by)  #

    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


def install(request):
    get_member_group()
    get_seller_group()

    get_prospectus(request.user)
    get_contract(request.user)

    get_event(settings.EVENT_ON_MEMBERSHIP_ADDED_TO_PERSON)
    get_event(settings.EVENT_ON_PERSON_CREATED)
    get_event(settings.EVENT_ON_PROSPECTUS_ADDED_BUT_NOT_MEMBERSHIP)

    member_group = settings.MEMBER_GROUP
    sales_group = settings.SALES_GROUP

    contract_form = settings.CONTRACT_FORM_NAME
    prospectus_form = settings.PROSPECTUS_FORM_NAME

    membership_added_to_person_event = settings.EVENT_ON_MEMBERSHIP_ADDED_TO_PERSON
    person_created_event = settings.EVENT_ON_PERSON_CREATED
    prospectus_added_but_not_membership_event = settings.EVENT_ON_PROSPECTUS_ADDED_BUT_NOT_MEMBERSHIP

    type_voucher_type, created = TypeVoucherProduct.objects.get_or_create(name=settings.VOUCHER_TYPE_PART_PAYMENT_NAME)

    return render(request, 'main/install.html', locals())
