from django.contrib import admin

# Register your models here.
from django.contrib.admin.models import LogEntry
from fullcalendar.models import Calendar
from main.models import Membership, Person, Event, Task, UserTask, Exercise, PersonMembership
from store.models import Card, TypeVoucherProduct, VoucherProduct, Company, RegimenType, CompanyTypeVoucher

admin.site.register(Membership)


class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'created_at')


admin.site.register(Person, PersonAdmin)

admin.site.register(LogEntry)
admin.site.register(Event)
admin.site.register(Calendar)


class TaskAdmin(admin.ModelAdmin):
    list_display = ('action', 'event', 'duration_frequency', 'duration_type',)


admin.site.register(Task, TaskAdmin)


class UserTaskAdmin(admin.ModelAdmin):
    list_display = ('task', 'date', 'object_repr')


admin.site.register(UserTask, UserTaskAdmin)
admin.site.register(Exercise)


class PersonMembershipAdmin(admin.ModelAdmin):
    list_display = ('membership', 'person', 'since', 'to', 'created_at',)


admin.site.register(PersonMembership, PersonMembershipAdmin)
admin.site.register(Card)
admin.site.register(TypeVoucherProduct)
admin.site.register(VoucherProduct)
admin.site.register(Company)
admin.site.register(RegimenType)
admin.site.register(CompanyTypeVoucher)
