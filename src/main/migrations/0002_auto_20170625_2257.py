# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0001_initial'),
        ('main', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='personmembership',
            name='voucher_product',
            field=models.ForeignKey(to='store.VoucherProduct', null=True),
        ),
        migrations.AddField(
            model_name='membership',
            name='product',
            field=models.ForeignKey(to='store.Product'),
        ),
        migrations.AddField(
            model_name='frozenpersonmembership',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='frozen_person_membership_created_by'),
        ),
        migrations.AddField(
            model_name='frozenpersonmembership',
            name='person_membership',
            field=models.ForeignKey(to='main.PersonMembership'),
        ),
        migrations.AddField(
            model_name='attendance',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='attendance',
            name='exercises',
            field=models.ManyToManyField(to='main.Exercise'),
        ),
        migrations.AddField(
            model_name='attendance',
            name='person_membership',
            field=models.ForeignKey(to='main.PersonMembership', null=True),
        ),
    ]
