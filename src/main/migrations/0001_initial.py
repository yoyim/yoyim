# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('forms', '0002_auto_20170625_2143'),
        ('accounts', '0002_auto_20170314_0746'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attendance',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'Asistencias',
                'verbose_name': 'Asistencia',
                'permissions': (('view_last_10_attendances', 'Can view last 10 attendances per person'), ('add_exercise_to_attendance', 'Can add exercise to attendance')),
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('key', models.SlugField(max_length=100, blank=None)),
                ('value', models.CharField(max_length=100, blank=None)),
            ],
            options={
                'verbose_name_plural': 'Eventos',
                'verbose_name': 'Evento',
                'permissions': (('views_tasks', 'Can view tasks assigned to him'),),
            },
        ),
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(verbose_name='nombre', max_length=20)),
                ('description', models.TextField(verbose_name='descripción')),
                ('is_active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Ejercicios',
                'verbose_name': 'Ejercicio',
            },
        ),
        migrations.CreateModel(
            name='FrozenPersonMembership',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('number', models.CharField(max_length=20)),
                ('since', models.DateField(verbose_name='desde')),
                ('to', models.DateField(verbose_name='hasta')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'People Memberships Frozen',
                'verbose_name': 'Person Membership Frozen',
            },
        ),
        migrations.CreateModel(
            name='Membership',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('duration_frequency', models.IntegerField(verbose_name='times', blank=None)),
                ('duration_type', models.IntegerField(choices=[(1, 'Días'), (2, 'Semanas'), (3, 'Meses'), (4, 'Años'), (5, 'Visitas')], verbose_name='mes,día,año', default=3)),
                ('limit_visits_per_day', models.IntegerField(choices=[(0, 'Ninguno'), (1, 'Una vez'), (2, 'Dos veces'), (3, 'Tres veces')], verbose_name='límite de visitas por día', default=0)),
                ('is_on_monday', models.BooleanField(verbose_name='lunes', default=True)),
                ('is_on_tuesday', models.BooleanField(verbose_name='martes', default=True)),
                ('is_on_wednesday', models.BooleanField(verbose_name='miércoles', default=True)),
                ('is_on_thursday', models.BooleanField(verbose_name='jueves', default=True)),
                ('is_on_friday', models.BooleanField(verbose_name='viernes', default=True)),
                ('is_on_saturday', models.BooleanField(verbose_name='sábado', default=True)),
                ('is_on_sunday', models.BooleanField(verbose_name='domingo', default=True)),
                ('is_active', models.BooleanField(verbose_name='esta activo', default=True)),
                ('is_promo', models.BooleanField(verbose_name='es promo', default=False)),
                ('date_end', models.DateField(verbose_name='termina el', blank=True, null=True)),
                ('from_time', models.TimeField()),
                ('to_time', models.TimeField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'membresías',
                'verbose_name': 'membresía',
                'permissions': (('view_memberships', 'Can see existing memberships'), ('view_seller_team', 'Can see seller team')),
            },
        ),
        migrations.CreateModel(
            name='PersonMembership',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('observation', models.TextField(verbose_name='objetivo de la persona', blank=True, null=True)),
                ('since', models.DateField(verbose_name='desde', default=django.utils.timezone.now)),
                ('to', models.DateField(verbose_name='hasta')),
                ('to_real', models.DateField(verbose_name='hasta', default=django.utils.timezone.now)),
                ('active', models.BooleanField(default=False)),
                ('contract_number', models.CharField(max_length=10)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='person_membership_created_by')),
                ('membership', models.ForeignKey(to='main.Membership')),
                ('person', models.ForeignKey(to='accounts.Person')),
            ],
            options={
                'verbose_name_plural': 'membresías',
                'verbose_name': 'membresías',
                'permissions': (('view_who_created_member', 'Can see who create a member'), ('view_last_10_memberships', 'Can view last 10 membership acquired by a  person'), ('report_added_people_by_users', 'Can see report about person added by users.')),
            },
        ),
        migrations.CreateModel(
            name='Prospectus',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('close_sale', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('entry', models.ForeignKey(to='forms.FormEntry')),
                ('person', models.ForeignKey(to='accounts.Person')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'prospecto',
                'verbose_name': 'prospecto',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('action', models.CharField(max_length=100, blank=None)),
                ('description', models.TextField(blank=True, null=True)),
                ('duration_frequency', models.IntegerField(blank=None)),
                ('duration_type', models.IntegerField(choices=[(1, 'Días'), (2, 'Semanas'), (3, 'Meses'), (4, 'Años'), (5, 'Visitas')], default=1)),
                ('default_done', models.BooleanField(default=False)),
                ('is_preventive', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True)),
                ('event', models.ForeignKey(to='main.Event', blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Tareas',
                'verbose_name': 'Tarea',
            },
        ),
        migrations.CreateModel(
            name='UserTask',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('object_id', models.CharField(verbose_name='id', max_length=200, blank=True, null=True)),
                ('object_repr', models.CharField(verbose_name='referencia', max_length=200)),
                ('date', models.DateField()),
                ('date_done', models.DateField(blank=True, null=True)),
                ('date_postpone', models.DateField(blank=True, null=True)),
                ('resume', models.TextField(blank=True)),
                ('done', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', blank=True, null=True)),
                ('task', models.ForeignKey(to='main.Task', blank=True, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Tareas de usuarios',
                'verbose_name': 'Tarea de usuario',
            },
        ),
    ]
