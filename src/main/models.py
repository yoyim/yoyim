from datetime import date, datetime

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Model, Sum
from django.db.models.fields.related import ForeignKey
from django.forms import TimeInput
from django.utils import timezone
from django.utils.translation import ugettext as _
from forms_builder.forms.models import FormEntry

from accounts.models import Person
from store.models import Product, VoucherProduct

DAYS = 1
WEEKS = 2
MONTHS = 3
YEARS = 4
VISITS = 5

DURATION_TYPES = (
    (DAYS, _('Days')),
    (WEEKS, _('Weeks')),
    (MONTHS, _('Months')),
    (YEARS, _('Years')),
    (VISITS, _('Visits')),
)

ANY = 0
ONCE = 1
TWICE = 2
THREE_TIMES = 3

ENTRIES_LIMIT_DAILY = (
    (ANY, _('Any')),
    (ONCE, _('ONCE')),
    (TWICE, _('TWICE')),
    (THREE_TIMES, _('THREE_TIMES')),
)


class Membership(Model):
    product = models.ForeignKey(Product)
    duration_frequency = models.IntegerField(_('times'), null=False, blank=None)
    duration_type = models.IntegerField(_('month,day,year'), choices=DURATION_TYPES, default=MONTHS)
    limit_visits_per_day = models.IntegerField(_('limit visits per day'), choices=ENTRIES_LIMIT_DAILY, default=ANY)
    is_on_monday = models.BooleanField(_('monday'), default=True)
    is_on_tuesday = models.BooleanField(_('tuesday'), default=True)
    is_on_wednesday = models.BooleanField(_('wednesday'), default=True)
    is_on_thursday = models.BooleanField(_('thursday'), default=True)
    is_on_friday = models.BooleanField(_('friday'), default=True)
    is_on_saturday = models.BooleanField(_('saturday'), default=True)
    is_on_sunday = models.BooleanField(_('sunday'), default=True)

    is_active = models.BooleanField(_('is active'), default=True)
    is_promo = models.BooleanField(_('is promo'), default=False)
    date_end = models.DateField(_('end at'), null=True, blank=True)

    from_time = models.TimeField()
    to_time = models.TimeField()
    # is_limit_time = models.BooleanField(default=False)
    # TODO implement period durant the day

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Membership')
        verbose_name_plural = _('Memberships')
        permissions = (
            ("view_memberships", "Can see existing memberships"),
            ("view_seller_team", "Can see seller team"),
        )

    def __str__(self):
        return self.product.name

    def get_count_members(self):
        return PersonMembership.objects.filter(membership=self).count()

    def allowed_days(self):
        days = ''
        if self.is_on_monday:
            days += 'L '
        if self.is_on_tuesday:
            days += 'M '
        if self.is_on_wednesday:
            days += 'W '
        if self.is_on_thursday:
            days += 'J '
        if self.is_on_friday:
            days += 'V '
        if self.is_on_saturday:
            days += 'S '
        if self.is_on_sunday:
            days += 'D '
        return days


class PersonMembership(Model):
    person = ForeignKey(Person)
    membership = ForeignKey(Membership)
    observation = models.TextField(_('target person'), blank=True, null=True)
    since = models.DateField(_('since'), default=timezone.now, null=False)
    to = models.DateField(_('to'), null=False)
    to_real = models.DateField(_('to'), null=False, default=timezone.now)
    active = models.BooleanField(default=False)
    voucher_product = models.ForeignKey(VoucherProduct, null=True)
    contract_number = models.CharField(max_length=10)

    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, related_name='person_membership_created_by')

    class Meta:
        verbose_name = _('Person Membership')
        verbose_name_plural = _('People Memberships')

        permissions = (
            ("view_who_created_member", "Can see who create a member"),
            ("view_last_10_memberships", "Can view last 10 membership acquired by a  person"),
            ("report_added_people_by_users", "Can see report about person added by users.")
        )


class FrozenPersonMembership(Model):
    number = models.CharField(null=False, blank=False, max_length=20)
    person_membership = ForeignKey(PersonMembership)
    since = models.DateField(_('since'), null=False)
    to = models.DateField(_('to'), null=False)

    created_at = models.DateTimeField(auto_now_add=True)

    created_by = models.ForeignKey(User, related_name='frozen_person_membership_created_by')

    class Meta:
        verbose_name = _('Person Membership Frozen')
        verbose_name_plural = _('People Memberships Frozen')


PersonMembership.freezing = property(lambda s: FrozenPersonMembership.objects.filter(person_membership=s))

User.count_sales_in_month = property(
    lambda s: PersonMembership.objects.filter(created_by=s, created_at__year=datetime.today().year,
                                              created_at__month=datetime.today().month).count())

User.total_sales_in_month = property(
    lambda s: PersonMembership.objects.filter(created_by=s, created_at__year=datetime.today().year,
                                              created_at__month=datetime.today().month).aggregate(
        Sum('membership__product__price'))['membership__product__price__sum'])


class Prospectus(Model):
    person = ForeignKey(Person)
    entry = ForeignKey(FormEntry)
    close_sale = models.BooleanField(default=False)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Prospectus')
        verbose_name_plural = _('Prospectus')


class Event(Model):
    key = models.SlugField(max_length=100, null=False, blank=None)
    value = models.CharField(max_length=100, null=False, blank=None)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        permissions = (
            ("views_tasks", "Can view tasks assigned to him"),
        )


class Task(Model):
    event = models.ForeignKey(Event, null=True, blank=True)
    action = models.CharField(max_length=100, null=False, blank=None)
    description = models.TextField(null=True, blank=True)
    duration_frequency = models.IntegerField(null=False, blank=None)
    duration_type = models.IntegerField(choices=DURATION_TYPES, default=1)
    default_done = models.BooleanField(default=False)
    is_preventive = models.BooleanField(default=False)

    active = models.BooleanField(default=True)

    def __str__(self):
        return self.event.value + ", " + self.action

    def natural_key(self):
        return (self.event.value, self.action, self.description)

    class Meta:
        verbose_name = _('Task')
        verbose_name_plural = _('Tasks')


class UserTask(Model):
    task = models.ForeignKey(Task, null=True, blank=True)
    user = models.ForeignKey(User)
    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.CharField(_('id'), max_length=200, blank=True, null=True)
    object_repr = models.CharField(_('reference'), max_length=200)
    date = models.DateField()
    date_done = models.DateField(null=True, blank=True)
    date_postpone = models.DateField(null=True, blank=True)
    resume = models.TextField(null=False, blank=True)
    done = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.task.__str__()

    class Meta:
        verbose_name = _('Tarea de usuario')
        verbose_name_plural = _('Tareas de usuarios')


class Exercise(Model):
    name = models.CharField(_('name'), max_length=20)
    description = models.TextField(_('descripcion'), )
    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Exercise')
        verbose_name_plural = _('Exercises')


class Attendance(Model):
    person_membership = models.ForeignKey(PersonMembership, null=True)
    exercises = models.ManyToManyField(Exercise)

    created_at = models.DateTimeField(auto_now_add=True)

    created_by = models.ForeignKey(User)

    class Meta:
        verbose_name = _('Attendance')
        verbose_name_plural = _('Attendances')
        permissions = (
            ("view_last_10_attendances", "Can view last 10 attendances per person"),
            ("add_exercise_to_attendance", "Can add exercise to attendance"),
        )

    def is_past_due(self):
        if date.today() > self.created_at.date():
            return True
        return False

    def __unicode__(self):
        return self.created_at


Person.get_active_membership_of_person = property(
    lambda s: PersonMembership.objects.filter(person=s, to__gt=timezone.now).first())

Person.last_attendance = property(
    lambda s: Attendance.objects.filter(person_membership__person=s).order_by('-created_at').first())
