/**
 * Created by bicho on 12/06/2015.
 */
var app = angular.module("MyApp", ['ngSanitize'], function ($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.config(function ($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

app.controller("calendarDemo", function ($scope, $sce, $filter, $http) {
    $scope.day = moment();
    $scope.userTasksAtDate = [];
    $scope.userTasksDoneAtDate = [];

    $scope.onChangeDay = function (day) {
        var date = $filter('date')(day.date.toDate(), "yyyy-MM-dd");
        $scope.userTasksAtDate = [];
        $scope.userTasksDoneAtDate = [];

        $http.post('/diary/get_tasks/', {date: date, done: true}).success(function (response) {
                angular.forEach(response, function (task) {
                    var userTask = {
                        name: task.fields.object_repr,
                        event: task.fields.task[0],
                        action: task.fields.task[1],
                        description: task.fields.task[2],
                        resume: task.fields.resume,
                        id: task.pk,
                        done: task.fields.done
                    };

                    if (userTask.done) {
                        $scope.userTasksDoneAtDate.push(userTask);
                    }else{
                        $scope.userTasksAtDate.push(userTask);
                    }
                });
            }
        );
    };

    $scope.checkAsDone = function (userTask, index) {
        $http.post('/diary/check_as_done/', userTask).success(function (response) {
            $scope.userTasksAtDate.splice(index, 1);
        });
    }
});

app.directive("calendar", function () {
    return {
        restrict: "E",
        templateUrl: "/static/main/angular/calendar.html",
        scope: {
            selected: "="
        },
        link: function (scope) {
//                    scope.selected = _removeTime(scope.selected || moment());
            scope.month = scope.selected.clone();

            var start = scope.selected.clone();
            start.date(1);
            _removeTime(start.day(0));

            _buildMonth(scope, start, scope.month);

            scope.select = function (day) {
                scope.selected = day.date;
                scope.$parent.onChangeDay(day);
            };

            scope.next = function () {
                var next = scope.month.clone();
                _removeTime(next.month(next.month() + 1).date(1));
                scope.month.month(scope.month.month() + 1);
                _buildMonth(scope, next, scope.month);
            };

            scope.previous = function () {
                var previous = scope.month.clone();
                _removeTime(previous.month(previous.month() - 1).date(1));
                scope.month.month(scope.month.month() - 1);
                _buildMonth(scope, previous, scope.month);
            };
        }
    };

    function _removeTime(date) {
        return date.day(0).hour(0).minute(0).second(0).millisecond(0);
    }

    function _buildMonth(scope, start, month) {
        scope.weeks = [];
        var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
        while (!done) {
            scope.weeks.push({ days: _buildWeek(date.clone(), month) });
            date.add(1, "w");
            done = count++ > 2 && monthIndex !== date.month();
            monthIndex = date.month();
        }
    }

    function _buildWeek(date, month) {
        var days = [];
        for (var i = 0; i < 7; i++) {
            days.push({
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date
            });
            date = date.clone();
            date.add(1, "d");
        }
        return days;
    }
});