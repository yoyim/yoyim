from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
import forms_builder.forms.urls

from gym import settings

urlpatterns = [
                  url(r'^', include('main.urls', namespace="main")),
                  url(r'^accounts/', include('accounts.urls', namespace="accounts")),
                  url(r'^members/', include('members.urls', namespace="members")),
                  url(r'^memberships/', include('memberships.urls', namespace="memberships")),
                  url(r'^diary/', include('diary.urls', namespace="diary")),
                  url(r'^store/', include('store.urls', namespace="store")),
                  url(r'^calendar/', include('fullcalendar.urls', namespace="calendar")),
                  url(r'^admin/', include(admin.site.urls)),
                  url(r'^forms/', include(forms_builder.forms.urls)),
                  url(r'^qualification/', include('qualification.urls', namespace='qualification'))
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler403 = 'main.views.custom_403'
