var app = angular.module('AppGymi', [], function ($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.factory('autoCompleteDataService', ['$http', function ($http) {
    return {
        getSource: function (pattern) {
            var data = [];
            return $http.post('/store/products-json/', {pattern: pattern}).then(function (response) {
                angular.forEach(response.data, function (item) {
                    data.push({
                        label: item.fields.name,
                        value: item.fields.name,
                        price: item.fields.price,
                        id: item.pk
                    });
                });
                return data;
            });
        }
    }
}]);

app.factory('assync', ['$http', function($http) {
    return {

        post: function(route, parameters) {
            return $http.post(route, parameters).then(function (response) {
                return response;
            });
        },
        get: function(route, parameters) {
            return $http.get(route, parameters).then(function (response) {
                return response;
            })
        }
    };
}]);

app.factory('dateFormat',['$filter', function($filter) {
    return function(date, format) {
        date = date || new Date();
        format = format || "yyyy-MM-dd";
        return $filter('date')(new Date(date), format);
    }
}]);

app.factory('numberToText', function(){
    return {
        units: function(number) {
            var numbers = {};
            numbers[0] = "cero";
            numbers[1] = "uno";
            numbers[2] = "dos";
            numbers[3] = "tres";
            numbers[4] = "cuatro";
            numbers[5] = "cinco";
            numbers[6] = "seis";
            numbers[7] = "siete";
            numbers[8] = "ocho";
            numbers[9] = "nueve";
            return numbers[number];
        },
        tens: function(number) {
            var unit = number%10;
            var dec = parseInt(number/10);
            if (dec == 0) {
                return this.units(unit);
            }
            var numbers = {};
            numbers[10] = "diez";
            numbers[11] = "once";
            numbers[12] = "doce";
            numbers[13] = "trece";
            numbers[14] = "catorce";
            numbers[15] = "quince";
            numbers[20] = "veinte";
            numbers[21] = "veinti";
            numbers[30] = "treinta";
            numbers[40] = "cuarenta";
            numbers[50] = "cincuenta";
            numbers[60] = "sesenta";
            numbers[70] = "setenta";
            numbers[80] = "ochenta";
            numbers[90] = "noventa";

            var result =  numbers[number];

            if (dec==1 && unit > 5 && unit <=9) {
                result = "dieci" + this.units(unit);
            }
            if (dec==2 && unit > 0 && unit <=9) {
                result = numbers[21]+this.units(unit)
            }
            if (dec >= 3 && unit > 0) {
                result = numbers[dec*10] + " y " + this.units(unit);
            }

            return result;
        },
        hundreds: function(number) {
            var cent = parseInt(number/100);
            var dec = number-cent*100;

            if (cent == 0) {
                return this.tens(dec);
            }

            var numbers = {};
            numbers[100] = "cien";
            numbers[101] = "ciento";
            numbers[500] = "quinientos";
            numbers[700] = "setecientos";
            numbers[900] = "novecientos";

            var nCent = (cent == 1 || cent == 0) ? "" : this.units(cent);
            var plural = (cent == 1 || cent == 0) ? "" : "s";
            var hund = nCent + ((cent == 0) ? "" : numbers[101]) + plural;
            if (cent == 5 || cent == 7 || cent == 9) {
                hund = numbers[cent * 100];
            }
            var result = hund;
            if (dec > 0) {
                result += " " + this.tens(dec);
            }
            return result;
        },
        thousands: function(number) {
            var mil = parseInt(number/1000);
            var dec = number-mil*1000;

            if (mil == 0) {
                return this.hundreds(dec);
            }

            if (mil > 0) {
                var numbers = {};
                numbers[1000] = "mil";

                var nMil = (mil == 1) ? "" : this.hundreds(mil) + " ";
                var result = nMil + numbers[1000] + " " + this.hundreds(dec);
            }
            return result;
        },
        millions: function(number) {
            var millo = parseInt(number/1000000);
            var mil = number-millo*1000000;

            if (millo == 0) {
                return this.thousands(mil);
            }

            var numbers = {};
            numbers[1000000] = "millon";

            var nMillo = this.thousands(millo);
            var plural = (millo==1)?"":"es";
            var result = nMillo + " " + numbers[1000000] + plural + " " + this.thousands(mil);

            return result;
        },
        cast: function(number) {
            if (number < 10) {
                return this.units(number);
            } else if (number < 100) {
                return this.tens(number);
            } else if (number < 1000) {
                return this.hundreds(number);
            }else if (number < 1000000) {
                return this.thousands(number);
            } else {
                return this.millions(number);
            }
        }

    };
});

app.directive('autoComplete', function (autoCompleteDataService) {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            elem.autocomplete({
                source: function (request, response) {
                    var term = request.term;
                    autoCompleteDataService.getSource(term).then(function (result) {
                        response(result);
                    });
                }, //from your service
                minLength: 3,
                change: function (event, ui) {
                    if (ui.item) {
                        scope.selectItemName(ui.item);
                    } else {
                        console.log('clear form');
                        scope.selectItemName(undefined);
                    }
                }
            });
        }
    };
});

app.directive('inputMask', function () {
    return {
        restrict: 'A',
        controller: ["$scope", "$element", function ($scope, $element) {
            var $elem = $($element);
            $elem.inputmask($scope.maskFormat);
            $scope.inputMaskT = $elem.attr('id');
        }]
    };
});

app.directive('checkTax', function () {
    return function (scope, element, attrs) {
        scope.voucherTypesTax = {};
        scope.voucherTypesSeries = {};
        scope.voucherTypesNames = {};
        scope.voucherTypesPercent = {};
        scope.selectedType = false;
        angular.forEach(element.find('option'), function (item, idx) {
            scope.voucherTypesTax[item.value] = item.dataset.tax;
            scope.voucherTypesSeries[item.value] = item.dataset.series;
            scope.voucherTypesNames[item.value] = item.dataset.names;
            scope.voucherTypesPercent[item.value] = parseFloat(item.dataset.percent);
        });
        scope.$watch('document.voucherType', function () {
            scope.selectedTax = scope.voucherTypesTax[scope.document.voucherType];
            scope.selectedSeries = scope.voucherTypesSeries[scope.document.voucherType];
            scope.selectedNames = scope.voucherTypesNames[scope.document.voucherType];
            scope.selectedPercent = scope.voucherTypesPercent[scope.document.voucherType];

            scope.haveTax = scope.selectedTax == 'True';
            scope.haveSeries = scope.selectedSeries == 'True';
            scope.haveNames = scope.selectedNames == 'True';
            scope.taxPercent = scope.selectedPercent;
            scope.calcValues();
        });
    }
});

app.controller('Taking', ["$scope", "$http", "assync", "numberToText", "dateFormat", function ($scope, $http, assync, numberToText, dateFormat) {
    //console.log('hola amiguitos');

    $scope.haveTax = false;
    $scope.haveSeries = true;
    $scope.haveNames = true;
    $scope.taxPercent = 0.0;

    $scope.companies = my_companies;
    $scope.getCompanies = function() {
        var tmp = [];
        for( var i=0; i < $scope.companies.length; i++) {
            var data = {
                'id': $scope.companies[i].id,
                'name': $scope.companies[i].name
            };
            tmp.push(data);
        }
        return tmp;
    };
    $scope.ruc = undefined;
    $scope.max_income = 0.00;
    $scope.current = 0.00;
    $scope.listVouchers = undefined;

    $scope.getVouchers = function (company_id) {
        var tmp = [];
        for (var i=0; i < $scope.companies.length; i++) {
            if ($scope.companies[i].id == company_id) {
                tmp = $scope.companies[i].vouchers;
                $scope.ruc = $scope.companies[i].ruc;
            }
        }
        return tmp;
    };

    $scope.updateInformation = function(company_id) {
        $scope.max_income = 0;
        $scope.current = 0;
        $scope.listVouchers = $scope.getVouchers(company_id);
    };

    $scope.listCompanies = $scope.getCompanies();
    /*
     * Start Document header Variables
     */
    $scope.document = {
        company: undefined,
        voucherType: undefined,
        series: undefined,
        number: undefined,
        date: dateFormat(undefined, "dd/MM/yyyy"),
        objectiveDocument: undefined,
        objectiveName: undefined,
        objectiveAddress: undefined,
        objectiveId: undefined
    };
    /*
     * End Document header Variables
     */

    /*
     * Start Client Set Variables
     */

    $scope.setNameInput = function ($val) {
        var name = String($val).replace(', ', ' , ');
        $scope.document.objectiveName = name;
    };

    $scope.setAddressInput = function ($val) {
        $scope.document.objectiveAddress = $val;
    };

    $scope.setObjectiveId = function ($val) {
        $scope.document.objectiveId = $val;
    };

    $scope.voucherId = 0;
    /*
     * End Client Variables
     */

    $scope.amountString = "";

    $scope.subtotal = 0.0;
    $scope.discount = 0.0;
    $scope.total = 0.0;
    $scope.tax = 0.0;
    $scope.part_payment = 0.0;

    $scope.save = false;

    $scope.rows = [];
    $scope.addItemButtonDisabled = true;

    $scope.generateText = function(number) {
        var numInt = parseInt(number);
        var decimal = parseInt(((number-numInt) * 100).toFixed(0));
        var fixDec = (decimal<10)?"0"+decimal:decimal;
        var text = numberToText.cast(numInt) + " con " + fixDec + "/100 nuevos soles";

        return text;
    };

    $scope.calcValues = function () {
        $scope.subtotal = 0.00;
        $scope.total = 0.00;
        $scope.discount = 0.00;

        angular.forEach($scope.rows, function (row) {
            $scope.subtotal += row.itemSubtotal;
            $scope.discount += row.itemDiscount;
            //$scope.total += row.itemTotal;
        });
        if ($scope.subtotal > 0) {
            $scope.tax = parseFloat(parseFloat(((($scope.taxPercent/100) + 1) * ($scope.subtotal - $scope.discount) ) - ($scope.subtotal - $scope.discount)).toFixed(2));

            $scope.total = parseFloat(parseFloat(($scope.subtotal + $scope.tax) - $scope.discount).toFixed(2));
        } else {
            $scope.tax = 0;
            $scope.total = 0;
        }
        $scope.amountString = $scope.generateText($scope.total);

    };

    $scope.updateValues = function() {
        $scope.max_income = $scope.document.voucherType.max;
        $scope.current = $scope.document.voucherType.current;
        $scope.haveTax = $scope.document.voucherType.have_tax;
        $scope.haveSeries = $scope.document.voucherType.have_series;
        $scope.haveNames = $scope.document.voucherType.name_required;
        $scope.taxPercent = parseFloat($scope.document.voucherType.percent);
        $scope.calcValues();
    };

    $scope.status = false;

    $scope.sendHeader = function () {
        return assync
            .post(routes.createHeader, $scope.document)
            .then(function (response) {
                $scope.status = true;
                $scope.voucherId = response.voucherId;
                return response;
            });
    };

    $scope.addProduct = function (row) {
        assync
            .post(routes.addProduct, row)
            .then(function (response) {
                if (response.data.success) {
                    $scope.rows.push(row);
                    $scope.calcValues();
                    $scope.clearItemForm();
                } else {
                    console.log(response.data.message);
                }
            });
    };

    $scope.errors = [];

    $scope.checkDocument = function() {
        if ($scope.document.voucherType != undefined) {
            if ($scope.haveSeries && ($scope.document.number == "" || $scope.document.number == undefined || $scope.document.series == "" || $scope.document.series == undefined)) {
                toastr.error("The document require a Serie-Number. Please complete this fields", "Series-Number is required");
                return false;
            }
            else if ($scope.haveNames && ($scope.document.objectiveName == undefined || $scope.document.objectiveName == "")) {
                toastr.error("Is necessary that you complete the name field for this type of document.", "Name is required.");
                return false;
            }
            return true;
        }
        toastr.error("You must select a voucher type.", "Voucher Type is required");
        return false;
    };

    $scope.addItem = function () {
        $scope.addItemButtonDisabled = true;

        if (!$scope.checkDocument()) {
            toastr.error("You need complete all fields");
            $scope.addItemButtonDisabled = false;
            return;
        }

        var alreadyExistsItemOnDetails = false;

        angular.forEach($scope.rows, function (row) {
            if (row.id == $scope.itemId) {
                alreadyExistsItemOnDetails = true;
            }
        });

        if (alreadyExistsItemOnDetails) {
            toastr.warning("There's already an item with this name");
            $scope.addItemButtonDisabled = false;
            return;
        }

        var subtotal = parseFloat($scope.itemQuantity * $scope.itemUnitaryPrice ).toFixed(2);
        var total = (subtotal - $scope.itemDiscount ).toFixed(2);

        if (subtotal <= 0) {
            $scope.addItemButtonDisabled = false;
            toastr.error("There's an error adding the item, the subtotal cannot be lees than zero");
            return;
        }

        var row = {
            id: $scope.itemId,
            itemName: $scope.itemName,
            itemQuantity: $scope.itemQuantity,
            itemPriceUnitary: parseFloat(parseFloat($scope.itemUnitaryPrice).toFixed(2)),
            itemSubtotal: parseFloat(subtotal),
            itemDiscount: parseFloat(parseFloat($scope.itemDiscount).toFixed(2)),
            itemTotal: parseFloat(total),
            voucherId: $scope.voucherId
        };

        /*
         * Verify if the voucher id exists, else send request to create.
     */
        if ($scope.voucherId == 0) {

            assync.post(routes.createHeader, $scope.document).then(function (response) {
                console.log(response);
                if (response.data.success) {
                    $scope.voucherId = response.data.voucherId;
                    row.voucherId = response.data.voucherId;
                    $scope.addProduct(row);
                }
            }, function(response){
                toastr.error("Problems in the server.<br>We are work in this.");
                $scope.addItemButtonDisabled = false;
            });
        } else {
            $scope.addProduct(row);
        }
    };

    $scope.selectItemName = function (item) {
        $scope.$apply(function () {
            if (item) {
                console.log(item);
                $scope.itemName = item.value;
                $scope.itemId = item.id;
                $scope.itemUnitaryPrice = item.price;
                $scope.addItemButtonDisabled = false;
            } else {
                $scope.clearItemForm();
            }
        });

    };

    $scope.removeItem = function (index) {
        var name = $scope.rows[index].itemName;
        toastr.info("Deleting voucher item '" + name + "'");

        assync
            .post(routes.delProduct, {
                id: $scope.rows[index].id,
                voucherId: $scope.rows[index].voucherId
            })
            .then(function(response) {
                if (response.data.success) {
                    $scope.rows.splice(index, 1);
                    $scope.calcValues();
                    toastr.success("The item '" + name + "' has been removed.");
                } else {
                    toastr.error(response.data.message);
                }
            });

    };

    $scope.clearItemForm = function () {
        $scope.itemName = '';
        $scope.itemUnitaryPrice = 0.0;
        $scope.itemDiscount = 0.0;
        $scope.itemQuantity = 0.0;
        $scope.addItemButtonDisabled = true;
    };

    $scope.itemDiscount = 0.0;
    $scope.itemUnitaryPrice = 0.0;

    $scope.sending = false;
    $scope.submit = function() {
        toastr.info("Sending voucher... <br \>One moment please.");
        $scope.sending = true;
        if (!$scope.checkDocument()) {
            toastr.error("You need complete all fields");
            return;
        } else {
            var info = {
                voucherId: $scope.voucherId,
                subtotal: $scope.subtotal,
                total: $scope.total,
                tax: $scope.tax,
                discount: $scope.discount,
                part_payment: $scope.part_payment
            };
            assync
                .post(routes.saveVoucher, info)
                .then(function (response) {
                    if (response.data.success) {
                        $scope.save = true;
                        toastr.success(response.data.message);
                        if (response.data.url) {
                            setTimeout(function () {
                                window.location.pathname = response.data.url;
                            }, 1500);
                        }
                    } else {
                        $scope.sending = false;
                        toastr.error(response.data.message);
                    }
                });
        }
    };

}]);

app.controller('ClientController', ["$scope", "$http", function ($scope, $http) {

    $scope.inputMaskT = "";
    $scope.placeholder = "";
    $scope.enable = false;
    $scope.floating = true;

    $scope.regFun = {
        complete: function() {
            $(this).parents('.form-group').addClass('has-success');
            console.log($(this).siblings('.fa'));
            $(this).siblings('.fa').removeClass('fa-user');
        }
    };

    $scope.regex = {
        company: {
            "regex": "([A-Za-z .])+",
            "oncomplete": function() {
                $(this).parents('.form-group').removeClass('has-error').addClass('has-success');
                $(this).siblings('.fa').removeClass('fa-user').removeClass('fa-times').addClass('fa-check');
            },
            "onincomplete": function() {
                $(this).parents('.form-group').removeClass('has-success').addClass('has-error');
                $(this).siblings('.fa').removeClass('fa-user').removeClass('fa-check').addClass('fa-times');
            },
            "oncleared": function () {
                $(this).parents('.form-group').removeClass('has-success').addClass('has-error');
                $(this).siblings('.fa').removeClass('fa-user').removeClass('fa-check').addClass('fa-times');
            }
        },
        person: {
            "regex": "(([A-Za-z]{1,30}\ [A-Za-z]{1,30})|([A-Za-z]{1,30}))((\,\ )|(\ \,)|(\,)|(\ \,)|(\ \,\ ))(([A-Za-z]{1,30}\ [A-Za-z]{0,30})|([A-Za-z]{1,30}))",
            "oncomplete": function () {
                $(this).parents('.form-group').removeClass('has-error').addClass('has-success');
                $(this).siblings('.fa').removeClass('fa-user').removeClass('fa-times').addClass('fa-check');
            },
            "onincomplete": function () {
                $(this).parents('.form-group').removeClass('has-success').addClass('has-error');
                $(this).siblings('.fa').removeClass('fa-user').removeClass('fa-check').addClass('fa-times');
            },
            "oncleared": function () {
                $(this).parents('.form-group').removeClass('has-success').addClass('has-error');
                $(this).siblings('.fa').removeClass('fa-user').removeClass('fa-check').addClass('fa-times');
            }
        }
    };

    $scope.maskFormat = $scope.regex.person;

    $scope.searchClient = function () {
        var document = $scope.document.objectiveDocument;
        if (document.length != 8 && document.length != 11) {
            toastr.error("DNI/RUC required 8(DNI) or 11(RUC) digits for search.", "Error in DNI/RUC search field.");
            return;
        }
        $http
            .post($scope.routeSearchClient, {
                search: $scope.document.objectiveDocument
            })
            .success(function (response) {
                if (response.success) {
                    $scope.setNameInput(response.name);
                    $scope.setAddressInput(response.address);
                    $scope.setObjectiveId(response.id);
                    $scope.enable = false;
                    $scope.floating = false;
                } else {
                    $scope.enable = true;
                    $scope.setNameInput("");
                    $scope.setAddressInput("");
                    $scope.setObjectiveId("");
                    $scope.floating = false;
                    if ($scope.document.objectiveDocument.length >= 10) {
                        //$scope.maskFormat = "{mask: '[ |*{2,60}]'}";
                        $scope.maskFormat = $scope.regex.company;
                        $scope.placeholder = "Empresa Ej: AQPUp S.A.C";
                    } else {
                        //$scope.maskFormat = "{mask: '(a{2,30})|(*{0,30}), a{2,40} a{0,40}'}";
                        $scope.maskFormat = $scope.regex.person;
                        $scope.placeholder = "Nombre Ej. Jhon, Doe";
                    }
                }
               $('#'+$scope.inputMaskT).inputmask('Regex',$scope.maskFormat);
               $('#'+$scope.inputMaskT).attr('placeholder', $scope.placeholder);
            })
            .error(function (error) {
                console.log(error);
            });
    };

}]);
