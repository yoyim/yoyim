var app = angular.module('AppGymi', [], function($httpProvider){
     $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.controller('SettingsController', ['$scope', '$http', function ($scope, $http) {
    $scope.voucherType = {
        name: '',
        have_tax: false,
        have_series: true,
        name_required: true,
        percent: parseFloat(0.00)
    };

    $scope.companyVType = {
        company: null,
        type_voucher: null,
        max_income: 0
    };

    $scope.listVoucherTypes = typeVouchersList;
    $scope.csrf = csrf;
    $scope.routesVoucherType = {
        delete: tvr_delete,
        edit: tvr_edit,
        new: tvr_new
        };

    $scope.routesCompanyVoucherType = ctv_routes;

    $scope.saveVoucherType = function() {
        $scope.$broadcast('sendVoucherType');
    };

    $scope.saveCompanyVoucherType = function() {
        $scope.$broadcast('sendCompanyVoucherType');
    };

}]);

app.controller('CategoriesController', ['$scope', '$http', function ($scope, $http) {

}]);

app.controller('VoucherTypeController', ['$scope', '$http', function ($scope, $http) {

    $scope.$on('sendVoucherType', function(e) {
        $scope.saveVoucherType();
    });

    $scope.saveVoucherType = function () {
        var route = (typeof $scope.$parent.voucherType.id === 'undefined') ? $scope.$parent.routesVoucherType.new : $scope.$parent.routesVoucherType.edit;
        $scope.$parent.voucherType.csrfmiddlewaretoken = $scope.$parent.csrf;
        $http
            .post(route, $scope.$parent.voucherType)
            .success(function (response) {
                if (response.success) {
                    $('.close-modal').click();
                    $scope.$parent.voucherType.id = response.id;
                    $scope.listVoucherTypes.push($scope.$parent.voucherType);
                    $scope.$parent.voucherType = {
                        name: '',
                        have_tax: false,
                        have_series: true,
                        name_required: true,
                        percent: 0.00
                    };
                    console.log(response.message);
                } else {
                    console.log(response.message);
                }
        });
    };

    $scope.delete = function(index) {

        var id = $scope.listVoucherTypes[index].id;
        $http
            .post($scope.routesVoucherType.delete, {id:id})
            .success(function(response) {
                if (response.success) {
                    $scope.listVoucherTypes.splice(index, 1);
                } else {
                    console.log(response.message);
                }
            });
    };

    $scope.edit = function(index) {
        $scope.$parent.voucherType = $scope.$parent.listVoucherTypes[index];
        $('#voucher-type-modal').click();
    }
}]);

app.controller('BusinessController', ['$scope','$http', function($scope, $http) {
    $scope.companies = companies;
    $scope.getCompanies = function() {
        var tmp = [];
        for( var i=0; i < $scope.companies.length; i++) {
            var data = {
                'id': $scope.companies[i].id,
                'name': $scope.companies[i].name
            };
            tmp.push(data);
        }
        return tmp;
    };
    $scope.company = null;
    $scope.ruc = null;
    $scope.total = 0.00;
    $scope.max = 0.00;

    $scope.listCompanies = $scope.getCompanies();

    $scope.getVouchers = function (company) {
        var tmp = [];
        if (company !== null) {
            for (var i = 0; i < $scope.companies.length; i++) {
                if ($scope.companies[i].id == company.id) {
                    tmp = $scope.companies[i].vouchers;
                    $scope.ruc = $scope.companies[i].ruc;
                }
            }
        }
        return tmp;
    };
    $scope.vouchers = $scope.getVouchers($scope.company);

    $scope.getAmounts = function () {
        $scope.total = $scope.max = 0.00;
        for (var i=0; i < $scope.vouchers.length; i++) {
            $scope.total += $scope.vouchers[i].current;
            $scope.max += $scope.vouchers[i].max;
        }
    };

    $scope.updateInformation = function(company_id) {
        $scope.vouchers = $scope.getVouchers(company_id);
        $scope.getAmounts();
    };

    $scope.$on('sendCompanyVoucherType', function(e) {
        $scope.saveCompanyVoucherType();
    });

    $scope.saveCompanyVoucherType = function () {
        var route = (typeof $scope.$parent.companyVType.id === 'undefined') ? $scope.$parent.routesCompanyVoucherType.new : $scope.$parent.routesCompanyVoucherType.edit;
        $scope.$parent.companyVType.csrfmiddlewaretoken = $scope.$parent.csrf;
        $http
            .post(route, $scope.$parent.companyVType)
            .success(function (response) {
                if (response.success) {
                    $('.close-modal').click();
                    $scope.$parent.companyVType = {
                        company: null,
                        type_voucher: null,
                        max_income: 0
                    };
                    $scope.companies = response.content;
                    $scope.listCompanies = $scope.getCompanies();
                    $scope.updateInformation($scope.company);
                    console.log(response.content);
                } else {
                    console.log(response.content);
                }
        });
    };

    $scope.edit = function($index) {
        $scope.$parent.companyVType = {
            id: $scope.vouchers[$index].id,
            company: parseInt($scope.company.id),
            type_voucher: $scope.vouchers[$index].id_vt,
            max_income: $scope.vouchers[$index].max
        };
        $('#company-voucher-modal').click();
    };

    $scope.delete = function($index) {
        var id = $scope.vouchers[$index].id;
        $http
            .post($scope.routesCompanyVoucherType.delete, {id:id})
            .success(function(response) {
                if (response.success) {
                    $scope.companies = response.content;
                    $scope.listCompanies = $scope.getCompanies();
                    $scope.updateInformation($scope.company);
                } else {
                    console.log(response.message);
                }
            });
    };
}]);