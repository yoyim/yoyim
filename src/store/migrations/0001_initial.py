# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20170314_0746'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=20)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='CategoryProduct',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=40, blank=None)),
                ('is_root', models.BooleanField(default=True)),
                ('level', models.IntegerField(null=True, blank=True, default=1)),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_inventoried', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('parent', models.ForeignKey(to='store.CategoryProduct', blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Categor&iacute;as',
                'verbose_name': 'Categor&iacute;a',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=100, blank=None)),
                ('ruc', models.CharField(max_length=20, blank=None)),
                ('address', models.CharField(max_length=200, blank=True, null=True)),
                ('telephone', models.CharField(max_length=30, blank=True, null=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_my_company', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('agent', models.ForeignKey(to='accounts.Person', blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Empresas',
                'verbose_name': 'Empresa',
            },
        ),
        migrations.CreateModel(
            name='CompanyTypeVoucher',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('max_income', models.DecimalField(decimal_places=2, max_digits=11, default=Decimal('0'))),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(to='store.Company')),
            ],
            options={
                'verbose_name_plural': 'Companies voucher types',
                'verbose_name': 'Company voucher type',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('code', models.CharField(max_length=100, default='000000')),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(to='store.Company', null=True)),
                ('person', models.ForeignKey(to='accounts.Person', null=True)),
            ],
            options={
                'verbose_name_plural': 'Clientes',
                'verbose_name': 'Cliente',
            },
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('observation', models.TextField(null=True, blank=True, default='')),
                ('input_amount', models.IntegerField(null=True, blank=True, default=0)),
                ('input_unitary_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('output_amount', models.IntegerField(null=True, blank=True, default=0)),
                ('output_unitary_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('balance_amount', models.IntegerField(null=True, blank=True, default=0)),
                ('balance_unitary_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Inventarios',
                'verbose_name': 'Inventario',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('code', models.CharField(max_length=100, unique=True)),
                ('name', models.CharField(max_length=100, blank=None)),
                ('price', models.DecimalField(decimal_places=2, max_digits=9, null=True, blank=True, default=Decimal('0.00'))),
                ('is_pack', models.BooleanField(default=False)),
                ('quantity', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0'))),
                ('unit_type', models.IntegerField(choices=[(1, 'Unidades'), (2, 'Kilogramos'), (3, 'Gramos'), (4, 'paquete'), (5, 'Cajas')], default=1)),
                ('stock', models.IntegerField(default=0)),
                ('min_stock', models.IntegerField(null=True, blank=True, default=0)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(to='store.CategoryProduct', related_name='category_product')),
                ('reference', models.ForeignKey(to='store.Product', blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Productos',
                'verbose_name': 'Producto',
            },
        ),
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('code', models.CharField(max_length=100, default='000000')),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(to='store.Company', null=True)),
                ('person', models.ForeignKey(to='accounts.Person', null=True)),
            ],
            options={
                'verbose_name_plural': 'Proveedores',
                'verbose_name': 'Proveedor',
            },
        ),
        migrations.CreateModel(
            name='RegimenType',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=120)),
                ('acronym', models.CharField(max_length=10)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Regimen Types',
                'verbose_name': 'Regimen Type',
            },
        ),
        migrations.CreateModel(
            name='TypeVoucherProduct',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=60, blank=None)),
                ('have_tax', models.BooleanField(default=False)),
                ('percent', models.DecimalField(decimal_places=2, max_digits=6, default=Decimal('0.00'))),
                ('have_series', models.BooleanField(default=True)),
                ('name_required', models.BooleanField(default=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_public', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Tipos de Comprobante',
                'verbose_name': 'Tipo de Comprobante',
            },
        ),
        migrations.CreateModel(
            name='VoucherProduct',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('series', models.CharField(max_length=32, blank=True, null=True)),
                ('number', models.CharField(max_length=32, blank=True, null=True)),
                ('date_issue', models.DateField(auto_now_add=True)),
                ('date_payed', models.DateField(auto_now=True)),
                ('subtotal', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('igv', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('total', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('discount', models.DecimalField(decimal_places=2, max_digits=10, null=True, blank=True, default=Decimal('0.00'))),
                ('observation', models.TextField(blank=True)),
                ('total_cashed', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('is_deleted', models.BooleanField(default=False)),
                ('status', models.IntegerField(choices=[(1, 'Pagado'), (2, 'Sin pagar'), (3, 'Anulado'), (4, 'Borrador'), (5, 'Pago Parcial'), (6, 'Parcialmente pagado')], default=2)),
                ('remaining_pay', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('is_input', models.BooleanField(default=True)),
                ('form_pay_cash_amount', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('form_pay_card_amount', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('form_pay_card_transaction_number', models.CharField(max_length='50', blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company_vouchers', models.ForeignKey(to='store.CompanyTypeVoucher', blank=True, null=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='voucher_product_created_by')),
                ('customer', models.ForeignKey(to='store.Customer', blank=True, null=True)),
                ('form_pay_card_type', models.ForeignKey(to='store.Card', blank=True, null=True)),
                ('in_payed_of', models.ForeignKey(to='store.VoucherProduct', blank=True, null=True)),
                ('my_company', models.ForeignKey(to='store.Company', blank=True, null=True)),
                ('provider', models.ForeignKey(to='store.Provider', blank=True, null=True)),
                ('type_voucher_product', models.ForeignKey(to='store.TypeVoucherProduct')),
            ],
            options={
                'verbose_name_plural': 'Comprobantes',
                'verbose_name': 'Comprobante',
                'permissions': (('view_cashbox', 'Puede ver el bot&oacute;n de tienda'),),
            },
        ),
        migrations.CreateModel(
            name='VoucherProductDetails',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('quantity', models.IntegerField(default=0)),
                ('unitary_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('discount', models.DecimalField(decimal_places=2, max_digits=10)),
                ('sub_total', models.DecimalField(decimal_places=2, max_digits=10)),
                ('total', models.DecimalField(decimal_places=2, max_digits=10, default=Decimal('0.00'))),
                ('active', models.BooleanField(default=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('product', models.ForeignKey(to='store.Product', related_name='voucher_product_details_product')),
                ('voucher_product', models.ForeignKey(to='store.VoucherProduct', related_name='voucher_product_details_voucher_product')),
            ],
            options={
                'verbose_name_plural': 'Detalles de Comprobantes',
                'verbose_name': 'Detalle del Comprobante',
            },
        ),
        migrations.AddField(
            model_name='inventory',
            name='product',
            field=models.ForeignKey(to='store.Product', related_name='inventory_product'),
        ),
        migrations.AddField(
            model_name='inventory',
            name='voucher_product',
            field=models.ForeignKey(related_name='inventory_voucher_product', to='store.VoucherProduct', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='companytypevoucher',
            name='type_voucher',
            field=models.ForeignKey(to='store.TypeVoucherProduct'),
        ),
        migrations.AddField(
            model_name='company',
            name='regimen_type',
            field=models.ForeignKey(to='store.RegimenType', null=True),
        ),
    ]
