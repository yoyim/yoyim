# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryProduct',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(blank=None, max_length=40)),
                ('is_root', models.BooleanField(default=True)),
                ('level', models.IntegerField(blank=True, default=1, null=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('parent', models.ForeignKey(blank=True, to='store.CategoryProduct', null=True)),
            ],
            options={
                'verbose_name': 'Categor&iacute;a',
                'verbose_name_plural': 'Categor&iacute;as',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(blank=None, max_length=100)),
                ('ruc', models.CharField(blank=None, max_length=20)),
                ('address', models.CharField(blank=True, max_length=200, null=True)),
                ('telephone', models.CharField(blank=True, max_length=30, null=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('agent', models.ForeignKey(blank=True, to='accounts.Person', null=True)),
            ],
            options={
                'verbose_name': 'Empresa',
                'verbose_name_plural': 'Empresas',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('code', models.CharField(max_length=100, default='000000')),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(to='store.Company', null=True)),
                ('person', models.ForeignKey(to='accounts.Person', null=True)),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
            },
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('observation', models.TextField(blank=True, default='', null=True)),
                ('input_amount', models.IntegerField(blank=True, default=0, null=True)),
                ('input_unitary_price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('output_amount', models.IntegerField(blank=True, default=0, null=True)),
                ('output_unitary_price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('balance_amount', models.IntegerField(blank=True, default=0, null=True)),
                ('balance_unitary_price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Inventario',
                'verbose_name_plural': 'Inventarios',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('code', models.CharField(unique=True, max_length=100)),
                ('name', models.CharField(blank=None, max_length=100)),
                ('price', models.DecimalField(null=True, default=Decimal('0.00'), blank=True, max_digits=9, decimal_places=2)),
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('code', models.CharField(max_length=100, unique=True)),
                ('name', models.CharField(blank=None, max_length=100)),
                ('price', models.DecimalField(blank=True, decimal_places=2, max_digits=9, default=Decimal('0.00'), null=True)),
                ('is_pack', models.BooleanField(default=False)),
                ('quantity', models.DecimalField(max_digits=10, default=Decimal('0'), decimal_places=2)),
                ('unit_type', models.IntegerField(choices=[(1, 'Unidades'), (2, 'Kilogramos'), (3, 'Gramos'), (4, 'paquete'), (5, 'Cajas')], default=1)),
                ('stock', models.IntegerField(default=0)),
                ('min_stock', models.IntegerField(null=True, default=0, blank=True)),
                ('min_stock', models.IntegerField(blank=True, default=0, null=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(to='store.CategoryProduct', related_name='category_product')),
                ('reference', models.ForeignKey(blank=True, to='store.Product', null=True)),
            ],
            options={
                'verbose_name': 'Producto',
                'verbose_name_plural': 'Productos',
            },
        ),
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('code', models.CharField(default='000000', max_length=100)),
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('code', models.CharField(max_length=100, default='000000')),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(to='store.Company', null=True)),
                ('person', models.ForeignKey(to='accounts.Person', null=True)),
            ],
            options={
                'verbose_name': 'Proveedor',
                'verbose_name_plural': 'Proveedores',
            },
        ),
        migrations.CreateModel(
            name='TypeVoucherProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(blank=None, max_length=60)),
                ('have_tax', models.BooleanField(default=False)),
                ('percent', models.DecimalField(max_digits=6, default=Decimal('0.00'), decimal_places=2)),
                ('have_series', models.BooleanField(default=True)),
                ('name_required', models.BooleanField(default=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_public', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Tipo de Comprobante',
                'verbose_name_plural': 'Tipos de Comprobante',
            },
        ),
        migrations.CreateModel(
            name='VoucherProduct',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('series', models.CharField(blank=True, max_length=32, null=True)),
                ('number', models.CharField(blank=True, max_length=32, null=True)),
                ('date_issue', models.DateField(auto_now_add=True)),
                ('date_payed', models.DateField(auto_now=True)),
                ('subtotal', models.DecimalField(max_digits=10, default=Decimal('0.00'), decimal_places=2)),
                ('igv', models.DecimalField(max_digits=10, default=Decimal('0.00'), decimal_places=2)),
                ('total', models.DecimalField(max_digits=10, default=Decimal('0.00'), decimal_places=2)),
                ('discount', models.DecimalField(blank=True, decimal_places=2, max_digits=10, default=Decimal('0.00'), null=True)),
                ('observation', models.TextField(blank=True)),
                ('total_cashed', models.DecimalField(max_digits=10, default=Decimal('0.00'), decimal_places=2)),
                ('is_deleted', models.BooleanField(default=False)),
                ('status', models.IntegerField(choices=[(1, 'Pagado'), (2, 'No pagado'), (3, 'Anulado'), (4, 'Borrador'), (5, 'Pago Parcial'), (6, 'Parcialmente pagado')], default=2)),
                ('remaining_pay', models.DecimalField(max_digits=10, default=Decimal('0.00'), decimal_places=2)),
                ('is_input', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='voucher_product_created_by')),
                ('customer', models.ForeignKey(blank=True, to='store.Customer', null=True)),
                ('in_payed_of', models.ForeignKey(blank=True, to='store.VoucherProduct', null=True)),
                ('provider', models.ForeignKey(blank=True, to='store.Provider', null=True)),
                ('type_voucher_product', models.ForeignKey(to='store.TypeVoucherProduct')),
            ],
            options={
                'verbose_name': 'Comprobante',
                'permissions': (('view_cashbox', 'Puede ver el bot&oacute;n de tienda'),),
                'verbose_name_plural': 'Comprobantes',
            },
        ),
        migrations.CreateModel(
            name='VoucherProductDetails',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('quantity', models.IntegerField(default=0)),
                ('unitary_price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('discount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('sub_total', models.DecimalField(max_digits=10, decimal_places=2)),
                ('total', models.DecimalField(max_digits=10, default=Decimal('0.00'), decimal_places=2)),
                ('active', models.BooleanField(default=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('product', models.ForeignKey(to='store.Product', related_name='voucher_product_details_product')),
                ('voucher_product', models.ForeignKey(to='store.VoucherProduct', related_name='voucher_product_details_voucher_product')),
            ],
            options={
                'verbose_name': 'Detalle del Comprobante',
                'verbose_name_plural': 'Detalles de Comprobantes',
            },
        ),
        migrations.AddField(
            model_name='inventory',
            name='product',
            field=models.ForeignKey(to='store.Product', related_name='inventory_product'),
        ),
        migrations.AddField(
            model_name='inventory',
            name='voucher_product',
            field=models.ForeignKey(related_name='inventory_voucher_product', blank=True, to='store.VoucherProduct', null=True),
        ),
    ]
