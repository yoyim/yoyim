from decimal import Decimal
import datetime
import json

from django.contrib.auth.decorators import login_required, permission_required
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, Sum
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.db import connection

from django.views.decorators.csrf import csrf_exempt

from accounts.models import Person
from main.functions import date_handler, monthdelta
from main.models import PersonMembership
from main.views import get_datatable_data
from store.forms import ProductForm, CategoryProductForm, \
    TypeVoucherProductForm, CompanyTypeVoucherForm
from store.functions import get_customer, get_provider, get_objective, generate_inventory, insert_register_inventory
from store.models import VoucherProductDetails, Product, CategoryProduct, TypeVoucherProduct, Customer, Company, \
    VoucherProduct, Inventory, Card, CompanyTypeVoucher
from gym import settings


@login_required
@permission_required('store.view_cashbox')
def home(request):
    return render(request, "store/home.html", locals())


@login_required
@permission_required('store.view_cashbox')
def new_incoming(request):
    voucher = "incoming"
    voucher_types = TypeVoucherProduct.objects.filter(is_deleted=False)
    date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
    date_end = monthdelta(date_start, 1)
    date_end = date_end - datetime.timedelta(days=1)
    my_companies = get_vouchers_x_companies(date_start, date_end)
    return render(request, "store/form.html", locals())


@login_required
@permission_required('store.view_cashbox')
def incoming(request):
    title = _('incoming')
    button = {
        'text': _('new') + " " + _('incoming'),
        'route': reverse('store:new_incoming')
    }
    columns_title = [_('series'), _('voucher'), _('date'), _('person'), _('company'), _('total'), _('cash'), _('card'),
                     _('amount card'), _('status')]
    columns_data = ['series_number', 'type_voucher_product__name', 'date_issue', 'person_name',
                    'customer__company__name', 'total_cashed', 'form_pay_cash_amount', 'form_pay_card_type__name',
                    'form_pay_card_amount', 'status']

    show_range_fields = True
    show_timepicker = True
    sum_colums = [5, 6, 8]
    total_column = min(sum_colums)
    number_colums = len(columns_title) - total_column
    range_cols = range(0, number_colums)
    filter_to_show_range_fields = ['_in_range_dates']
    filter_options = [
        ('_all', _('all')),
        ('_today', _('today')),
        ('_yesterday', _('yesterday')),
        ('_this_week', _('this week')),
        ('_in_range_dates', _('in range dates'))
    ]
    json_service = reverse('store:incoming_report_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def incoming_report_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request)

    rows = VoucherProduct.objects \
        .extra(select={
        'person_name': '"accounts_person"."name"||\', \'||"accounts_person"."surname"',
        'series_number': '"store_voucherproduct"."series"||\'-\'||"store_voucherproduct"."number"'
    }) \
        .values('id',
                'series_number',
                'type_voucher_product__name',
                'date_issue',
                'status',
                'customer__company__name',
                'customer__person__name',
                'person_name',
                'total_cashed',
                'form_pay_cash_amount',
                'form_pay_card_type__name',
                'form_pay_card_amount') \
        .filter(is_input=True, is_deleted=False)

    today = datetime.datetime.today()

    if query == '_in_range_dates':
        rows = rows.filter(created_at__range=(range_since, range_to))
    elif query == '_today':
        today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
        today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        rows = rows.filter(date_issue__range=(today_min, today_max))
    elif query == '_yesterday':
        day = datetime.date.today() - datetime.timedelta(1)
        day_min = datetime.datetime.combine(day, datetime.time.min)
        day_max = datetime.datetime.combine(day, datetime.time.max)
        rows = rows.filter(date_issue__range=(day_min, day_max))
    elif query == '_this_week':
        day = datetime.date.today() - datetime.timedelta(7)
        day_min = datetime.datetime.combine(day, datetime.time.min)
        rows = rows.filter(date_issue__range=(day_min, datetime.datetime.today()))

    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]

    for row in rows:
        row['status'] = VoucherProduct.STATUS_VOUCHER[row['status'] - 1][1]
        row['series_number'] = '<a href="' + reverse('store:voucher_view', args=[row['id']]) + '">' + (
            row['series_number'] if row['series_number'] is not None else "0000-0000000") + '</a>'

    result = {}
    result['data'] = list(rows)
    result['iTotalRecords'] = rows.count()
    result['iTotalDisplayRecords'] = iTotalDisplayRecords

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required
@permission_required('store.view_cashbox')
def expenses(request):
    title = _('expenses')
    button = {
        'text': _('new') + " " + _('expenses'),
        'route': reverse('store:new_expenses')
    }
    columns_title = [_('series'), _('voucher'), _('date'), _('person'), _('company'), _('total'), _('cash'), _('card'),
                     _('amount card'), _('status')]
    columns_data = ['series_number', 'type_voucher_product__name', 'date_issue', 'person_name',
                    'customer__company__name', 'total_cashed', 'form_pay_cash_amount', 'form_pay_card_type__name',
                    'form_pay_card_amount', 'status']

    show_range_fields = True
    show_timepicker = True
    sum_colums = [5, 7]
    total_column = min(sum_colums)
    number_colums = len(columns_title) - total_column
    range_cols = range(0, number_colums)
    filter_to_show_range_fields = ['_in_range_dates']
    filter_options = [
        ('_all', _('all')),
        ('_today', _('today')),
        ('_yesterday', _('yesterday')),
        ('_this_week', _('this week')),
        ('_in_range_dates', _('in range dates'))
    ]
    json_service = reverse('store:expenses_report_json')
    return render(request, 'main/layout_report.html', locals())


@csrf_exempt
@login_required()
def expenses_report_json(request):
    end, field_order_by, query, start, range_since, range_to = get_datatable_data(request,
                                                                                  date_format=settings.DATE_FORMAT)

    rows = VoucherProduct.objects \
        .extra(select={
        'person_name': '"accounts_person"."name"||\', \'||"accounts_person"."surname"',
        'series_number': '"store_voucherproduct"."series"||\'-\'||"store_voucherproduct"."number"'
    }) \
        .values('id',
                'series_number',
                'type_voucher_product__name',
                'status',
                'date_issue',
                'person_name',
                'provider__company__name',
                'provider__person__name',
                'total_cashed',
                'form_pay_cash_amount',
                'form_pay_card_type__name',
                'form_pay_card_amount') \
        .filter(is_input=False, is_deleted=False)

    today = datetime.datetime.today()

    if query == '_in_range_dates':
        rows = rows.filter(created_at__range=(range_since, range_to))
    elif query == '_today':
        today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
        today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        rows = rows.filter(date_issue__range=(today_min, today_max))
    elif query == '_yesterday':
        day = datetime.date.today() - datetime.timedelta(1)
        day_min = datetime.datetime.combine(day, datetime.time.min)
        day_max = datetime.datetime.combine(day, datetime.time.max)
        rows = rows.filter(date_issue__range=(day_min, day_max))
    elif query == '_this_week':
        day = datetime.date.today() - datetime.timedelta(7)
        day_min = datetime.datetime.combine(day, datetime.time.min)
        rows = rows.filter(date_issue__range=(day_min, datetime.datetime.today()))
    rows = rows.order_by(field_order_by)

    iTotalDisplayRecords = rows.count()

    if end:
        rows = rows[start:end]
    for row in rows:
        row['status'] = VoucherProduct.STATUS_VOUCHER[row['status'] - 1][1]
        row['series_number'] = '<a href="' + reverse('store:voucher_view', args=[row['id']]) + '">' + (
            row['series_number'] if row['series_number'] is not None else "0000-0000000") + '</a>'

    result = {}
    result['data'] = list(rows)
    result['iTotalRecords'] = rows.count()
    result['iTotalDisplayRecords'] = iTotalDisplayRecords

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@login_required
@permission_required('store.view_cashbox')
def new_expenses(request):
    voucher = _("expenses")
    voucher_types = TypeVoucherProduct.objects.filter(is_deleted=False)
    date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
    date_end = monthdelta(date_start, 1)
    date_end = date_end - datetime.timedelta(days=1)
    my_companies = get_vouchers_x_companies(date_start, date_end)

    return render(request, "store/form.html", locals())


@login_required
@permission_required('store.view_cashbox')
def products(request):
    title = _('products')
    products = Product.objects.filter(is_deleted=False)
    return render(request, "store/products.html", locals())


@login_required
@permission_required('store.view_cashbox')
def new_product(request):
    title = _('new') + " " + _('product')
    if request.POST:
        product_form = ProductForm(request.POST)
        if product_form.is_valid():
            product = product_form.save()

            return redirect(reverse('store:products'))
    else:
        product_form = ProductForm()
        category_form = CategoryProductForm()
    return render(request, "store/products/new.html", locals())


@login_required
@permission_required('store.view_cashbox')
def edit_product(request, product_id):
    title = _('edit') + " " + _('product')
    product = Product.objects.get(pk=product_id)
    if request.POST:
        product_form = ProductForm(request.POST, instance=product)
        mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST['reference'] = request.POST['reference'] if request.POST.get('is_pack') else None
        request.POST['quantity'] = request.POST['quantity'] if request.POST.get('is_pack') else Decimal(0.00)
        request.POST._mutable = mutable
        if product_form.is_valid():
            product_save = product_form.save(commit=False)
            product_save.save()

            return redirect(reverse('store:products'))
    else:
        product_form = ProductForm(instance=product)
        category_form = CategoryProductForm()
    return render(request, "store/products/new.html", locals())


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def delete_product(request):
    response = {}
    if request.POST:
        product = Product.objects.get(pk=request.POST.get('id'))
        product.is_deleted = True
        product.save()

        response['success'] = True
        response['message'] = (_("product") + " " + _("deleted")).title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required
@permission_required('store.view_cashbox')
def convert_product(request, product_id):
    product_parent = get_object_or_404(Product, pk=product_id)
    product_child = product_parent.reference
    if request.POST:
        amount = request.POST.get('quantity')
        parent = insert_register_inventory(id_product=product_id,
                                           amount=amount,
                                           unitary_price=product_parent.price,
                                           observation=(_("Convert to ") + product_child.name),
                                           is_input=False)
        child_amount = int(amount) * int(product_parent.quantity)
        child = insert_register_inventory(id_product=product_child.id,
                                          amount=child_amount,
                                          unitary_price=product_child.price,
                                          observation=(_("Convert from ") + product_parent.name),
                                          is_input=True)
        return redirect(reverse('store:products'))
    return render(request, "store/products/convert.html", locals())


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def get_products(request):
    if request.POST:
        pattern = request.POST.get('pattern')
        s_json = serializers.serialize('json', Product.objects.filter(
            Q(name__icontains=pattern) | Q(code__icontains=pattern),
            is_deleted=False),
                                       fields=('pk', 'name', 'price'))
    else:
        s_json = {}
    return HttpResponse(s_json, content_type='application/json')


@login_required
@permission_required('store.view_cashbox')
def new_category(request):
    response = {}
    level = 1
    if request.method == 'POST':
        parent = request.POST.get('parent')
        if parent != '0' and parent:
            cat = CategoryProduct.objects.get(pk=int(parent))
            level = cat.level + 1
        else:
            parent = None

        mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST['parent'] = parent
        request.POST['level'] = level
        request.POST._mutable = mutable

        form_category = CategoryProductForm(request.POST)
        if form_category.is_valid():
            category = form_category.save()
            response['success'] = True
            response['id'] = category.id
            response['name'] = category.name
            response['level'] = level
            response['type'] = "default"
        else:
            response['success'] = False
            response['message'] = _("invalid values").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required
@permission_required('store.view_cashbox')
def edit_category(request):
    response = {}
    if request.POST:
        instance = get_object_or_404(CategoryProduct, id=request.POST.get('id'))
        level = instance.level
        parent = request.POST.get('parent')
        if parent == '0':
            parent = None
        mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST['parent'] = parent
        request.POST['level'] = level
        request.POST._mutable = mutable
        form_category = CategoryProductForm(request.POST, instance=instance)
        if form_category.is_valid():
            form_category.save()
            response['success'] = True
            response['id'] = request.POST.get('id')
            response['level'] = level
            response['name'] = request.POST.get('name')
            response['type'] = "default"
        else:
            response['success'] = False
            response['message'] = _("invalid arguments").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required
@permission_required('store.view_cashbox')
def delete_category(request):
    response = {}
    status = 200
    if request.POST:
        gid = request.POST.get('id').split(':')
        category = get_object_or_404(CategoryProduct, id=gid[0])

        children = CategoryProduct.objects.filter(parent=category)

        if children.count() > 0:
            response['success'] = False
            response['message'] = _("you can't delete a parent category.").capitalize()
            status = 404
        else:
            category.is_deleted = True
            category.save()
            response['success'] = True
            response['message'] = _("deleted").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(
        json.dumps(response),
        content_type="application/json",
        status=status
    )


@login_required
@permission_required('store.view_cashbox')
def categories(request):
    response = []
    level = 1
    general = {}
    if request.GET:
        gid = request.GET.get('id')
        if gid:
            if gid != '0':
                cid = int(gid)
                cat = CategoryProduct.objects.get(pk=cid)
                if cat:
                    level = cat.level + 1
            else:
                cid = None

            categories_i = CategoryProduct.objects.filter(parent__id__exact=cid, is_deleted=False)
            tmp = {}
            for category in categories_i:
                tmp['id'] = category.id
                tmp['parent'] = gid
                tmp['name'] = category.name
                tmp['level'] = level
                response.append(tmp)
                tmp = {}
    general['nodes'] = response
    return HttpResponse(json.dumps(general), content_type="application/json")


@login_required
@permission_required('store.view_cashbox')
def new_type_voucher(request):
    response = {}
    if request.POST:
        form_type_voucher = TypeVoucherProductForm(request.POST)
        if form_type_voucher.is_valid():
            type_voucher = form_type_voucher.save()
            response['success'] = True
            response['id'] = type_voucher.id
            response['name'] = type_voucher.name
            response['have_tax'] = type_voucher.have_tax
            response['percent'] = float(type_voucher.percent)
            response['name_required'] = type_voucher.name_required
            response['have_series'] = type_voucher.have_series
        else:
            response['success'] = False
            response['message'] = _("invalid arguments").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
@login_required()
@permission_required('store.view_cashbox')
def delete_type_voucher(request):
    response = {}
    if request.POST:
        vt_id = request.POST.get('id')
        if vt_id:
            voucher_product = TypeVoucherProduct.objects.get(pk=vt_id)
            voucher_product.is_deleted = True
            voucher_product.save()

            response['success'] = True
            response['message'] = _("success delete").title()
        else:
            response['success'] = False
            response['message'] = _("invalid id.").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()

    return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
@permission_required('store.view_cashbox')
def edit_type_voucher(request):
    response = {}
    if request.POST:
        vt_id = request.POST.get('id');
        instance = get_object_or_404(TypeVoucherProduct, id=vt_id)
        form_type_voucher = TypeVoucherProductForm(request.POST, instance=instance)
        if form_type_voucher.is_valid():
            form_type_voucher.save()
            response['success'] = True
            response['message'] = _("edited success").title()
        else:
            response['success'] = False
            response['message'] = _("invalid arguments").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
@login_required()
@permission_required('store.view_cashbox')
def delete_company_type_voucher(request):
    response = {}
    if request.POST:
        vt_id = request.POST.get('id')
        if vt_id:
            c_voucher = CompanyTypeVoucher.objects.get(pk=vt_id)
            c_voucher.is_deleted = True
            c_voucher.save()
            date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
            date_end = monthdelta(date_start, 1)
            date_end = date_end - datetime.timedelta(days=1)

            response['success'] = True
            response['message'] = _("success delete").title()
            response['content'] = get_vouchers_x_companies(date_start, date_end)
        else:
            response['success'] = False
            response['message'] = _("invalid id.").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()

    return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
@permission_required('store.view_cashbox')
def new_company_type_voucher(request):
    response = {}
    if request.POST:
        company = request.POST.get('company')
        type_voucher = request.POST.get('type_voucher')
        instance = CompanyTypeVoucher.objects.filter(company=company, type_voucher=type_voucher)
        if instance.count() == 0:
            form_c_type_voucher = CompanyTypeVoucherForm(request.POST)
        else:
            form_c_type_voucher = CompanyTypeVoucherForm(request.POST, instance=instance.first())
        if form_c_type_voucher.is_valid():

            if instance.count() == 0:
                c_tv = form_c_type_voucher.save()
            else:
                c_tv = form_c_type_voucher.save(commit=False)
                c_tv.is_deleted = False
                c_tv.save()
            date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
            date_end = monthdelta(date_start, 1)
            date_end = date_end - datetime.timedelta(days=1)
            response['success'] = True
            response['id'] = c_tv.id
            response['content'] = get_vouchers_x_companies(date_start, date_end)
        else:
            response['success'] = False
            response['message'] = _("invalid arguments").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
@permission_required('store.view_cashbox')
def edit_company_type_voucher(request):
    response = {}
    if request.POST:
        vt_id = request.POST.get('id');
        instance = get_object_or_404(CompanyTypeVoucher, id=vt_id)
        form_type_voucher = CompanyTypeVoucherForm(request.POST, instance=instance)
        if form_type_voucher.is_valid():
            form_type_voucher.save()
            date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
            date_end = monthdelta(date_start, 1)
            date_end = date_end - datetime.timedelta(days=1)
            response['success'] = True
            response['message'] = _("edited success").title()
            response['content'] = get_vouchers_x_companies(date_start, date_end)
        else:
            response['success'] = False
            response['message'] = _("invalid arguments").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
@permission_required('store.view_cashbox')
def inventory(request):
    title = _('inventory')
    inventory = Inventory.objects.filter(is_deleted=False)
    return render(request, "store/inventory.html", locals())


def get_vouchers_x_companies(date_start, date_end):
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM voucher_x_company(%s,%s)', [date_start, date_end])
    all_result = cursor.fetchall()
    result = []
    for row in all_result:
        head = {
            'id': row[1],
            'name': row[2],
            'ruc': row[3],
            'vouchers': []
        }
        vouchers = {
            'id': row[0],
            'name': row[5],
            'id_vt': row[4],
            'current': float(row[6]) if not row[6] is None else float(0.00),
            'max': float(row[7]) if not row[7] is None else float(0.00),
            'have_tax': row[10],
            'percent': float(row[11]),
            'have_series': row[12],
            'name_required': row[13]
        }
        exist = False
        for item in result:
            if item['id'] == row[1]:
                exist = True
                item['vouchers'].append(vouchers)
                break
        if not exist:
            head['vouchers'].append(vouchers)
            result.append(head)
    return result


@login_required
@permission_required('store.view_cashbox')
def settings_route(request):
    title = _('settings')
    categories_i = CategoryProduct.objects.filter(is_deleted=False)
    voucher_type = TypeVoucherProduct.objects.filter(is_deleted=False)
    voucher_type_form = TypeVoucherProductForm()
    category_form = CategoryProductForm()
    company_type_voucher = CompanyTypeVoucherForm()
    date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
    date_end = monthdelta(date_start, 1)
    date_end = date_end - datetime.timedelta(days=1)
    my_companies = get_vouchers_x_companies(date_start.isoformat(), date_end.isoformat())
    return render(request, "store/settings.html", locals())


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def search_client(request):
    response = {}
    if request.POST:
        to_search = request.POST.get('search')
        found = True
        is_customer = True
        if 8 <= len(to_search) <= 11:
            client = Customer.objects.filter(code=to_search)
            if client.count() == 0:
                is_customer = False
                client = Person.objects.filter(dni=to_search)
                if client.count() == 0:
                    client = Company.objects.filter(ruc=to_search)
                    if client.count() == 0:
                        found = False

            if found:
                if is_customer:
                    customer = client.first()
                    if not customer.person:
                        client = Company.objects.get(pk=int(customer.company.id))
                    else:
                        client = Person.objects.get(pk=int(customer.person.id))
                else:
                    client = client.first()
                response['success'] = True
                response['id'] = client.id
                response['name'] = client.name + ("" if 8 < len(to_search) else " , " + client.surname)
                response['address'] = client.address
            else:
                response['success'] = False
                response['message'] = _('not found').title()
        else:
            response['success'] = False
            response['message'] = _('invalid arguments').title()
    else:
        response['success'] = False
        response['message'] = _('invalid request').title()
        response['request'] = request.GET.get('search')

    return HttpResponse(json.dumps(response), content_type='application/json')


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def add_product(request):
    response = {}
    if request.POST:
        values = request.POST
        if values.get('voucherId') == "":
            response['success'] = False
            response['message'] = _("no voucher id").title()
        else:
            product_search = VoucherProductDetails.objects.filter(voucher_product=values.get('voucherId'),
                                                                  product=values.get('id'),
                                                                  active=True)
            if product_search.count() != 0:
                product_search.update(active=False)
            voucher = VoucherProduct.objects.get(pk=values.get('voucherId'))
            product = Product.objects.get(pk=values.get('id'))
            voucher_detail = VoucherProductDetails(voucher_product=voucher,
                                                   product=product,
                                                   quantity=int(values.get('itemQuantity')),
                                                   unitary_price=Decimal(values.get('itemPriceUnitary')),
                                                   discount=Decimal(values.get('itemDiscount')),
                                                   sub_total=Decimal(values.get('itemSubtotal')),
                                                   total=Decimal(values.get('itemTotal')))
            voucher_detail.save()

            response['success'] = True
            response['voucherId'] = voucher_detail.id
            response['message'] = _("save success").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()

    return HttpResponse(json.dumps(response), content_type='application/json')


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def del_product(request):
    response = {}
    if request.POST:
        if request.POST.get('id') and request.POST.get('voucherId'):
            product = get_object_or_404(Product, pk=request.POST.get('id'))
            voucher = get_object_or_404(VoucherProduct, pk=request.POST.get('voucherId'))
            try:
                voucher_detail = VoucherProductDetails.objects.get(product=product, voucher_product=voucher)
                voucher_detail.is_deleted = True
                voucher_detail.save()

                response['message'] = _("Perfect! the items has been deleted.")
                response['success'] = True
            except ObjectDoesNotExist:
                response['message'] = _("Sorry, but the item doesn't exist")
                response['success'] = False
        else:
            response['success'] = False
            response['message'] = _("invalid parameters").title()
    else:
        response['success'] = False
        response['message'] = _("invalid request").title()

    return HttpResponse(json.dumps(response), content_type='application/json')


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def create_header(request):
    response = {}
    errors = []
    path = request.META.get('HTTP_REFERER')  # request.path
    is_person = True
    is_input = True
    if request.POST:
        values = {
            'companyVoucherT': request.POST.get('voucherType[id]'),
            'series': request.POST.get('series'),
            'number': request.POST.get('number'),
            'voucherType': request.POST.get('voucherType[id_vt]'),
            'date': request.POST.get('date'),
            'objectiveId': request.POST.get('objectiveId'),
            'objectiveName': request.POST.get('objectiveName') if request.POST.get('objectiveName') else " , ",
            'objectiveDocument': request.POST.get('objectiveDocument'),
            'objectiveAddress': request.POST.get('objectiveAddress')
        }

        if values['voucherType']:
            voucher_type = TypeVoucherProduct.objects.get(pk=values['voucherType'])
            if voucher_type.have_series and (not values['series'] or not values['number']):
                errors.append(_('The voucher required series-number'))
            if voucher_type.name_required and (not values['objectiveName']):
                errors.append(_('The voucher required name'))

            if len(errors) == 0:
                user = request.user
                if ',' not in values['objectiveName']:
                    is_person = False
                if not values['objectiveId']:
                    if not voucher_type.name_required:
                        values['objectiveName'] = " , " if not values['objectiveName'] else values['objectiveName']
                        values['objectiveAddress'] = "" if not values['objectiveAddress'] else values[
                            'objectiveAddress']
                        values['objectiveDocument'] = "" if not values['objectiveDocument'] else values[
                            'objectiveDocument']

                    objective = get_objective(obj_name=values['objectiveName'],
                                              obj_number=values['objectiveDocument'],
                                              obj_address=values['objectiveAddress'],
                                              created_by=user)
                    values['objectiveId'] = objective[0]
                    is_person = objective[1]
                    values['objectiveDocument'] = objective[2]

                if values['objectiveId']:
                    customer = None
                    provider = None

                    if "incoming" in path:
                        customer = get_customer(values['objectiveDocument'], values['objectiveId'], is_person)
                    else:
                        provider = get_provider(values['objectiveDocument'], values['objectiveId'], is_person)
                        is_input = False

                    type_voucher = TypeVoucherProduct.objects.get(pk=values['voucherType'])

                    if not values['series'] or not values['number']:
                        if voucher_type.have_series:
                            errors.append(_('Voucher series and number is required'))
                        else:
                            values['series'] = '00000'
                            values['number'] = '0000000'

                    if len(errors) == 0:
                        company_voucher = CompanyTypeVoucher.objects.get(pk=values['companyVoucherT'])
                        voucher_product = VoucherProduct(series=values['series'],
                                                         number=values['number'],
                                                         my_company=company_voucher.company,
                                                         company_vouchers=company_voucher,
                                                         type_voucher_product=type_voucher,
                                                         date_issue=values['date'],
                                                         date_payed=values['date'],
                                                         customer=customer,
                                                         provider=provider,
                                                         created_by=user,
                                                         is_input=is_input)
                        voucher_product.save()
                        response['success'] = True
                        response['message'] = _("save success").title()
                        response['voucherId'] = voucher_product.id
                    else:
                        response['success'] = False
                else:
                    response['success'] = False
            else:
                response['success'] = False
                errors.append(_("Information incomplete."))
        else:
            response['success'] = False
            errors.append(_("You need insert Documents Series"))

    else:
        response['success'] = False
        response['message'] = _("invalid request").title()
    response['errors'] = errors
    return HttpResponse(json.dumps(response), content_type='application/json')


def copy_voucher_details(voucher_obj, voucher_des):
    voucher_details = VoucherProductDetails.objects.filter(voucher_product=voucher_obj)

    for vd in voucher_details:
        tmp = vd
        tmp.pk = None
        tmp.voucher_product = voucher_des
        tmp.save()


def generate_part_payment(voucher_product, part_payment):
    check = VoucherProduct.objects.filter(in_payed_of=voucher_product)
    voucher_type, created = TypeVoucherProduct.objects.get_or_create(name=settings.VOUCHER_TYPE_PART_PAYMENT_NAME)
    voucher = VoucherProduct.objects.get(pk=voucher_product.pk)
    voucher.pk = None
    voucher.number += "-" + str(check.count() + 1)
    voucher.type_voucher_product = voucher_type
    voucher.total_cashed = Decimal(part_payment)
    voucher.status = voucher.PAY_PARTIAL
    voucher.in_payed_of = voucher_product
    voucher.save()
    voucher_product.status = voucher.PARTIALLY_PAID
    voucher_product.remaining_pay = voucher_product.total_cashed - voucher.total_cashed
    voucher_product.save()
    copy_voucher_details(voucher_product, voucher)


@csrf_exempt
@login_required
@permission_required('store.view_cashbox')
def save_voucher(request):
    response = {}
    if request.POST:
        response['success'] = True

        if request.POST.get('part_payment'):
            voucher_id = request.POST.get('voucherId')
            subtotal = request.POST.get('subtotal')
            discount = request.POST.get('discount')
            tax = request.POST.get('tax')
            total = request.POST.get('total')
            part_payment = Decimal(request.POST.get('part_payment'))

            voucher_product = VoucherProduct.objects.get(pk=voucher_id)
            voucher_product.subtotal = Decimal(subtotal)
            voucher_product.igv = Decimal(tax)
            voucher_product.discount = Decimal(discount)
            voucher_product.total_cashed = Decimal(total)
            voucher_product.status = voucher_product.UNPAID
            voucher_product.form_pay_cash_amount = Decimal(total)
            voucher_product.save()

            generate_inventory(voucher_product)

            if part_payment > Decimal('0'):
                generate_part_payment(voucher_product, part_payment)

            response['message'] = _("save success").title()
            response['url'] = reverse('store:voucher_view', args=[voucher_id])
    else:
        response['success'] = False
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
@permission_required('store.view_cashbox')
def voucher_view(request, voucher_id):
    voucher_product = VoucherProduct.objects.get(pk=voucher_id)
    voucher_product.save()
    type_voucher_type, created = TypeVoucherProduct.objects.get_or_create(name=settings.VOUCHER_TYPE_PART_PAYMENT_NAME)
    voucher_part_payment = type_voucher_type.id
    voucher_details = VoucherProductDetails.objects.filter(voucher_product=voucher_product, is_deleted=False)
    part_payments = VoucherProduct.objects.filter(in_payed_of=voucher_product)
    date_start = datetime.date(datetime.date.today().year, datetime.date.today().month, 1)
    # TODO review if is better get_vouchers_x_companies by date_start__month and date_start__year
    date_end = monthdelta(date_start, 1)
    date_end = date_end - datetime.timedelta(days=1)
    company_voucher = get_vouchers_x_companies(date_start, date_end)
    cards = Card.objects.filter(active=True)
    voucher_types = TypeVoucherProduct.objects.filter(is_public=True, is_deleted=False)

    return render(request, "store/voucher_view.html", locals())


@csrf_exempt
@login_required
def edit_voucher(request):
    response = {
        'status': False,
        'message': None,
        'refresh': False
    }
    if request.POST:
        if request.POST.get('pk'):
            if request.POST.get('name'):
                name = request.POST.get('name')
                value = request.POST.get('value')
                pk = request.POST.get('pk')
                voucher_product = get_object_or_404(VoucherProduct, pk=pk)
                if name == 'voucher_type':
                    voucher_type = get_object_or_404(TypeVoucherProduct, pk=value)
                    company_voucher = get_object_or_404(CompanyTypeVoucher,
                                                        type_voucher=voucher_type,
                                                        company=voucher_product.my_company)
                    if voucher_type.have_tax:
                        tax = Decimal((((voucher_type.percent / 100) + 1) * (
                            voucher_product.subtotal - voucher_product.discount)) - (
                                          voucher_product.subtotal - voucher_product.discount))
                    else:
                        tax = Decimal(0.00)
                    voucher_product.igv = tax
                    voucher_product.total_cashed = (voucher_product.subtotal + tax) - voucher_product.discount
                    voucher_product.type_voucher_product = voucher_type
                    voucher_product.company_vouchers = company_voucher
                    voucher_product.save()
                    response['refresh'] = True
                elif name == 'series':
                    voucher_product.series = value
                elif name == 'number':
                    voucher_product.number = value
                elif name == 'date_issue':
                    voucher_product.date_issue = value
                elif name == 'discount':
                    value = Decimal(value.replace(',', '.'))
                    voucher_product.discount = value
                    voucher_product.total_cashed = voucher_product.total_cashed - value
                elif name == 'part_payment':
                    if not voucher_product.number:
                        response['message'] = _('please, complete voucher number')
                        return HttpResponseBadRequest(json.dumps(response), content_type='application/json')
                    value = Decimal(value.replace(',', '.'))
                    if value != Decimal('0.00'):
                        generate_part_payment(voucher_product, value)
                    response['refresh'] = True
                elif name == 'form_pay_cash_amount':
                    value = Decimal(value.replace(',', '.'))
                    if (value + voucher_product.form_pay_card_amount) > voucher_product.total_cashed:
                        response['message'] = _('the amount cash + amount card is more than total payment')
                        return HttpResponseBadRequest(json.dumps(response), content_type='application/json')
                    voucher_product.form_pay_cash_amount = value
                    voucher_product.save()
                elif name == 'form_pay_card_amount':
                    value = Decimal(value.replace(',', '.'))
                    if (value + voucher_product.form_pay_cash_amount) > voucher_product.total_cashed:
                        response['message'] = _('the amount cash + amount card is more than total payment')
                        return HttpResponseBadRequest(json.dumps(response), content_type='application/json')
                    voucher_product.form_pay_card_amount = value
                    voucher_product.save()
                elif name == 'form_pay_card_type':
                    if value is '':
                        voucher_product.form_pay_card_type = None
                    else:
                        voucher_product.form_pay_card_type = get_object_or_404(Card, pk=value)
                    voucher_product.save()
                elif name == 'form_pay_card_transaction_number':
                    voucher_product.form_pay_card_transaction_number = value
                    voucher_product.save()
                elif name == 'company':
                    company = get_object_or_404(Company, pk=value)
                    voucher_product.my_company = company
                    response['refresh'] = True;
                voucher_product.save()
                response['status'] = True
                response['message'] = _('save complete').title()
    return HttpResponse(json.dumps(response), content_type='application/json')


@login_required
def pay_voucher(request, voucher_id):
    voucher = get_object_or_404(VoucherProduct, pk=voucher_id)
    if voucher.remaining_pay != Decimal('0.00'):
        generate_part_payment(voucher, voucher.remaining_pay)
    voucher.date_payed = datetime.date.today()
    voucher.status = voucher.PAID
    voucher.save()

    # to active membership, delete this lines to use in other modules
    try:
        person_membership = PersonMembership.objects.get(voucher_product=voucher)
        person_membership.active = True
        person_membership.save()
    except PersonMembership.DoesNotExist:
        pass

    return redirect(reverse('store:voucher_view', args={voucher_id}))
