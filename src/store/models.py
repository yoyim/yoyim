from decimal import Decimal

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _

from accounts.models import Person


class Card(models.Model):
    name = models.CharField(max_length=20)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class RegimenType(models.Model):
    name = models.CharField(max_length=120)
    acronym = models.CharField(max_length=10)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Regimen Type')
        verbose_name_plural = _('Regimen Types')


class Company(models.Model):
    name = models.CharField(max_length=100, null=False, blank=None)
    regimen_type = models.ForeignKey(RegimenType, null=True)
    ruc = models.CharField(max_length=20, null=False, blank=None)
    address = models.CharField(max_length=200, null=True, blank=True)
    agent = models.ForeignKey(Person, null=True, blank=True)
    telephone = models.CharField(max_length=30, null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_my_company = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')


class Customer(models.Model):
    code = models.CharField(max_length=100, default="000000")
    person = models.ForeignKey(Person, null=True)
    company = models.ForeignKey(Company, null=True)
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')


class Provider(models.Model):
    code = models.CharField(max_length=100, default="000000")
    person = models.ForeignKey(Person, null=True)
    company = models.ForeignKey(Company, null=True)
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Provider')
        verbose_name_plural = _('Providers')


class CategoryProduct(models.Model):
    name = models.CharField(max_length=40, null=False, blank=None)
    is_root = models.BooleanField(default=True)
    parent = models.ForeignKey("self", blank=True, null=True)
    level = models.IntegerField(default=1, blank=True, null=True)
    is_deleted = models.BooleanField(default=False)
    is_inventoried = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Product(models.Model):
    Units = 1
    Kilograms = 2
    Grams = 3
    Packs = 4
    Boxes = 5

    UNITS_TYPE = (
        (Units, _("Units")),
        (Kilograms, _("Kilograms")),
        (Grams, _("Grams")),
        (Packs, _("Packs")),
        (Boxes, _("Boxes"))
    )

    code = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100, null=False, blank=None)
    price = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True, default=Decimal('0.00'))
    category = models.ForeignKey(CategoryProduct, related_name="category_product")
    is_pack = models.BooleanField(default=False)
    reference = models.ForeignKey("self", blank=True, null=True)
    quantity = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal(0.00))
    unit_type = models.IntegerField(choices=UNITS_TYPE, default=Units)
    stock = models.IntegerField(default=0)
    min_stock = models.IntegerField(default=0, null=True, blank=True)
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class TypeVoucherProduct(models.Model):
    name = models.CharField(max_length=60, null=False, blank=None)
    have_tax = models.BooleanField(default=False)
    percent = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    have_series = models.BooleanField(default=True)
    name_required = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    is_public = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Voucher Type')
        verbose_name_plural = _('Vouchers Types')


class CompanyTypeVoucher(models.Model):
    company = models.ForeignKey(Company)
    type_voucher = models.ForeignKey(TypeVoucherProduct)
    max_income = models.DecimalField(max_digits=11, decimal_places=2, default=Decimal(0.00))
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Company voucher type')
        verbose_name_plural = _('Companies voucher types')


class VoucherProduct(models.Model):
    PAID = 1
    UNPAID = 2
    ANNULLED = 3
    DRAFT = 4
    PAY_PARTIAL = 5
    PARTIALLY_PAID = 6

    STATUS_VOUCHER = (
        (PAID, _("Paid")),
        (UNPAID, _("Unpaid")),
        (ANNULLED, _("Annulled")),
        (DRAFT, _("Draft")),
        (PAY_PARTIAL, _("Pay Partial")),
        (PARTIALLY_PAID, _("Partially Paid"))
    )
    my_company = models.ForeignKey(Company, blank=True, null=True)
    company_vouchers = models.ForeignKey(CompanyTypeVoucher, null=True, blank=True)
    series = models.CharField(max_length=32, null=True, blank=True)
    number = models.CharField(max_length=32, null=True, blank=True)
    type_voucher_product = models.ForeignKey(TypeVoucherProduct)
    date_issue = models.DateField(auto_now_add=True)
    date_payed = models.DateField(auto_now=True)
    customer = models.ForeignKey(Customer, null=True, blank=True)
    provider = models.ForeignKey(Provider, null=True, blank=True)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    igv = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    total = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    discount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=Decimal('0.00'))
    observation = models.TextField(blank=True)
    total_cashed = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    is_deleted = models.BooleanField(default=False)
    status = models.IntegerField(choices=STATUS_VOUCHER, default=UNPAID)
    in_payed_of = models.ForeignKey('self', blank=True, null=True)
    remaining_pay = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))

    # Check if is input of money
    is_input = models.BooleanField(default=True)

    form_pay_cash_amount = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    form_pay_card_amount = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    form_pay_card_type = models.ForeignKey(Card, null=True, blank=True)
    form_pay_card_transaction_number = models.CharField(max_length='50', blank=True, null=True)

    created_by = models.ForeignKey(User, related_name="voucher_product_created_by")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Voucher')
        verbose_name_plural = _('Vouchers')

        permissions = (
            ("view_cashbox", _("Can view the button store")),
        )

    def __str__(self):
        if not self.series:
            self.series = ''
        if not self.number:
            self.number = ''
        return self.series + '-' + self.number


class VoucherProductDetails(models.Model):
    voucher_product = models.ForeignKey(VoucherProduct, related_name="voucher_product_details_voucher_product")
    product = models.ForeignKey(Product, related_name="voucher_product_details_product")
    quantity = models.IntegerField(default=0, null=False, blank=False)
    unitary_price = models.DecimalField(max_digits=10, decimal_places=2)
    discount = models.DecimalField(max_digits=10, decimal_places=2)
    sub_total = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Voucher Detail')
        verbose_name_plural = _('Vouchers Details')


class Inventory(models.Model):
    product = models.ForeignKey(Product, related_name="inventory_product")
    voucher_product = models.ForeignKey(VoucherProduct, related_name="inventory_voucher_product", null=True, blank=True)
    observation = models.TextField(default='', blank=True, null=True)
    input_amount = models.IntegerField(default=0, null=True, blank=True)
    input_unitary_price = models.DecimalField(max_digits=10, decimal_places=2)
    output_amount = models.IntegerField(default=0, null=True, blank=True)
    output_unitary_price = models.DecimalField(max_digits=10, decimal_places=2)
    balance_amount = models.IntegerField(default=0, null=True, blank=True)
    balance_unitary_price = models.DecimalField(max_digits=10, decimal_places=2)
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Inventory')
        verbose_name_plural = _('Inventories')
