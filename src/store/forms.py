from django.utils.translation import ugettext as _

from main.functions import add_class
from store.models import VoucherProduct, VoucherProductDetails, Product, CategoryProduct, TypeVoucherProduct, \
    CompanyTypeVoucher, Company

__author__ = 'jona'

from django.forms import ModelForm, ModelChoiceField, TextInput, CheckboxInput, NumberInput
from django import forms


class CategoryModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class GeneralModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class CategoryProductForm(ModelForm):
    form_meta = {'title': _('category_product')}
    name = forms.CharField(max_length=60)
    parent = CategoryModelChoiceField(queryset=CategoryProduct.objects.all(), empty_label="", required=False)

    class Meta:
        model = CategoryProduct
        exclude = ("",)

    def __init__(self, *args, **kwargs):
        super(CategoryProductForm, self).__init__(*args, **kwargs)
        add_class(self, ['name', 'parent'])


class ProductForm(ModelForm):
    form_meta = {'title': _('product')}
    code = forms.CharField(max_length=100)
    name = forms.CharField(max_length=100)
    category = CategoryModelChoiceField(queryset=CategoryProduct.objects.all(), empty_label="")
    reference = GeneralModelChoiceField(queryset=Product.objects.all(), empty_label="", required=False)

    class Meta:
        model = Product
        exclude = ("stock", "min_stock")

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        add_class(self, ['code', 'name', 'price', 'category', 'unit_type', 'quantity', 'reference'])


class VoucherProductForm(ModelForm):
    form_meta = {'title': _('voucher'), }
    series = forms.CharField(max_length=30)
    number = forms.CharField(max_length=20)
    type_voucher_product = GeneralModelChoiceField(queryset=TypeVoucherProduct.objects.all(), empty_label="")

    class Meta:
        model = VoucherProduct
        exclude = ("",)

    def __init__(self, *args, **kwargs):
        super(VoucherProductForm, self).__init__(*args, **kwargs)
        add_class(self, ['series', 'number', 'type_voucher_product'])


class VoucherProductDetailForm(ModelForm):
    form_meta = {'title': _('voucher details'), }

    class Meta:
        model = VoucherProductDetails
        exclude = ("",)


class TypeVoucherProductForm(ModelForm):
    form_meta = {'title': _('type voucher')}

    class Meta:
        model = TypeVoucherProduct
        exclude = ("",)
        fields = ('name', 'have_tax', 'have_series', 'name_required', 'percent')
        widgets = {
            'name': TextInput(attrs={'ng-model': 'voucherType.name', 'value': ''}),
            'have_tax': CheckboxInput(attrs={'ng-model': 'voucherType.have_tax'}),
            'have_series': CheckboxInput(attrs={'ng-model': 'voucherType.have_series', 'checked': 'checked'}),
            'name_required': CheckboxInput(attrs={'ng-model': 'voucherType.name_required', 'checked': 'checked'}),
            'percent': NumberInput(attrs={'ng-model': 'voucherType.percent', 'value': '0.00'})
        }

    def __init__(self, *args, **kwargs):
        super(TypeVoucherProductForm, self).__init__(*args, **kwargs)
        add_class(self, ['name', 'have_tax', 'have_series', 'name_required', 'percent'])


class CompanyTypeVoucherForm(ModelForm):
    form_meta = {'title': _('Company type voucher')}

    company = GeneralModelChoiceField(Company.objects.filter(is_my_company=True), empty_label="")
    type_voucher = GeneralModelChoiceField(TypeVoucherProduct.objects.filter(is_deleted=False, is_public=True), empty_label="")

    class Meta:
        model = CompanyTypeVoucher
        exclude = ("",)
        fields = ('company', 'type_voucher', 'max_income')
        widgets = {
            'max_income': NumberInput(attrs={'ng-model': 'companyVType.max_income'}),
        }

    def __init__(self, *args, **kwargs):
        super(CompanyTypeVoucherForm, self).__init__(*args, **kwargs)
        add_class(self, ['company', 'type_voucher', 'max_income'])
        self.fields['company'].widget.attrs.update(
            {
                'ng-model': 'companyVType.company'
            }
        )
        self.fields['type_voucher'].widget.attrs.update(
            {
                'ng-model': 'companyVType.type_voucher'
            }
        )