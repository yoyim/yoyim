from decimal import Decimal
from main.models import Person
from store.models import Customer, Provider, Company, VoucherProductDetails, Inventory, Product, VoucherProduct
from django.utils.translation import ugettext as _

__author__ = 'jona'


def get_array_dict_of_post_array(post, fields):
    fields_array = {}
    x = -1
    for field in fields:
        _field = field + '[]'
        if not post.get(_field):
            return None
        d = dict(post)[_field]
        fields_array[field] = d
        if x == -1:
            x = d.__len__()
        if x != d.__len__():
            raise Exception(_("fields size aren't equals"))

    if x == -1:
        return None

    response = []
    for k in range(x):
        r = {}
        for field in fields:
            r[field] = fields_array.get(field)[k]
        response.append(r)
    return response


def get_customer(code, cid, is_person):
    person = None
    company = None

    if is_person:
        person = Person.objects.get(pk=cid)
    else:
        company = Company.objects.get(pk=cid)

    customer, created = Customer.objects.get_or_create(code=code, person=person, company=company)

    return customer


def get_provider(code, cid, is_person):
    person = None
    company = None

    if is_person:
        person = Person.objects.get(pk=cid)
    else:
        company = Company.objects.get(pk=cid)

    provider, created = Provider.objects.get_or_create(code=code, person=person, company=company)

    return provider


def get_person(dni, name, surname, address, created_by):
    name = _("Unknown") if name in "  " else name
    surname = _("Unknown") if surname in "  " else surname
    dni = "00000000" if not dni else dni

    person, created = Person.objects.get_or_create(dni=dni,
                                                   name=name,
                                                   surname=surname,
                                                   defaults={
                                                       'address': address,
                                                       'created_by': created_by
                                                   })
    return person.id, dni


def get_company(ruc, name, address):
    company, created = Company.objects.get_or_create(ruc=ruc,
                                                     name=name,
                                                     defaults={'address': address})
    company_id = company.id
    return company_id


def get_objective(obj_name, obj_number, obj_address, created_by):
    is_person = True
    document = obj_number
    if ',' not in obj_name:
        is_person = False

    if is_person:
        fullname = obj_name.split(',')
        name = fullname[0]
        surname = fullname[1]

        objective_id, document = get_person(dni=obj_number,
                                            name=name,
                                            surname=surname,
                                            address=obj_address,
                                            created_by=created_by)
    else:
        objective_id = get_company(ruc=obj_number,
                                   name=obj_name,
                                   address=obj_address)

    return objective_id, is_person, document


def get_last_amount(id_product):
    product = Product.objects.get(pk=id_product)
    inventory = Inventory.objects.filter(product=product).order_by('-id')
    if inventory.count() > 0:
        inv = inventory[0]
        amount = inv.balance_amount
    else:
        amount = 0
    return amount


def update_stock_product(product, amount):
    product.stock = amount
    product.save()

    return product


def insert_register_inventory(id_product, amount, unitary_price, id_voucher=None, observation=None, is_input=True):
    product = Product.objects.get(pk=id_product)

    if product.category.is_inventoried:
        last_amount = int(get_last_amount(id_product))
        amount = int(amount)
        input_amount = output_amount = 0
        input_unitary_price = output_unitary_price = Decimal(0.0)
        if is_input:
            balance_amount = last_amount + amount
            input_amount = amount
            input_unitary_price = unitary_price
        else:
            balance_amount = last_amount - amount
            output_amount = amount
            output_unitary_price = unitary_price

        if id_voucher:
            voucher_product = VoucherProduct.objects.get(pk=id_voucher)
        else:
            voucher_product = None
        inventory = Inventory(product=product,
                              voucher_product=voucher_product,
                              observation=observation,
                              input_amount=input_amount,
                              input_unitary_price=input_unitary_price,
                              output_amount=output_amount,
                              output_unitary_price=output_unitary_price,
                              balance_amount=balance_amount,
                              balance_unitary_price=Decimal(0.0)
                              )
        inventory.save()

        pr = update_stock_product(product, inventory.balance_amount)
    else:
        pr = None
    return pr


def generate_inventory(voucher_product):
    voucher_details = VoucherProductDetails.objects.filter(voucher_product=voucher_product)
    for voucher_detail in voucher_details:
        product = insert_register_inventory(id_product=voucher_detail.product.id,
                                            amount=voucher_detail.quantity,
                                            unitary_price=voucher_detail.unitary_price,
                                            id_voucher=voucher_product.id,
                                            observation="",
                                            is_input=(not voucher_product.is_input)
                                            )
