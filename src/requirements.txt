django==1.8
Pillow==2.8.1
sorl-thumbnail==12.2
python-dateutil==2.4.2
-e git+git://github.com/bichocj/django-forms-builder.git#egg=django-forms-builder
pytz
psycopg2