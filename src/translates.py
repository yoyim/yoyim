import os

def getDirectories(path):
	listDir = []
	listFiles = os.listdir(path)

	for itemFile in listFiles:
		if os.path.isdir(itemFile):
			listdir.append(itemFile)

	return listDir

def isInDirectory(value, path):
	listFiles = os.listdir(path)
	return (value in listFiles)

def executeCommand():
	with open("manage.py") as f:
		code = compile(f.read(), "", 'exec')
		exec(code)
