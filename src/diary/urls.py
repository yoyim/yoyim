from django.conf.urls import url, patterns

urlpatterns = patterns('diary.views',
    url(r'^home/', 'home', name='home'),
    url(r'^new/', 'new', name='new'),
    url(r'^tasks/todo', 'tasks_todo', name='tasks_todo'),
    url(r'^tasks/done', 'tasks_done', name='tasks_done'),
    url(r'^tasks/postpone', 'tasks_postpone', name='tasks_postpone'),
    url(r'^get_tasks/', 'get_tasks', name='get_tasks'),
    url(r'^check_as_done/', 'check_as_done', name='check_as_done'),
    url(r'^check_as_postpone/', 'check_as_postpone', name='check_as_postpone'),
)
