__author__ = 'bicho'
from django.forms import ModelForm
from django.utils.translation import ugettext as _

from main.models import UserTask


class UserTaskForm(ModelForm):
    form_meta = {'title': _('new task'), 'legend': _('task')}

    class Meta:
        model = UserTask
        fields = ('object_repr', 'resume', 'date', 'done',)

    def __init__(self, *args, **kwargs):
        super(UserTaskForm, self).__init__(*args, **kwargs)
        self.fields['date'].widget.format = '%d/%m/%Y'

        # at the same time, set the input format on the date field like you want it:
        self.fields['date'].input_formats = ['%d/%m/%Y']
