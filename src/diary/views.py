from datetime import datetime
import json

from django.contrib.auth.decorators import login_required, permission_required
from django.core import serializers
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from diary.forms import UserTaskForm
from main.models import UserTask


def home(request):
    if request.POST:
        pass
    else:
        user_tasks = UserTask.objects.filter(user=request.user, date=datetime.now())
    return render(request, 'diary/home.html', locals())


@permission_required('main.views_tasks')
def tasks_todo(request):
    title = _('to do')
    user_tasks = UserTask.objects.filter(user=request.user, date_postpone__isnull=True, done=False,
                                         active=True).order_by('date')[:50]
    user_tasks_type = 'todo'
    return render(request, 'diary/tasks.html', locals())


def tasks_postpone(request):
    title = _('postponed')
    user_tasks = UserTask.objects.filter(user=request.user, done=False, date_postpone__isnull=False, active=True) \
        .order_by('-date_postpone')
    user_tasks_type = 'postpone'
    show_date_postpone = True
    return render(request, 'diary/tasks.html', locals())


def tasks_done(request):
    title = _('done')
    user_tasks = UserTask.objects.filter(user=request.user, done=True, active=True).order_by('-date_done')
    user_tasks_type = 'done'
    # hide_done_link = True
    return render(request, 'diary/tasks.html', locals())


def get_tasks(request):
    date = request.POST.get('date')

    tasks = UserTask.objects.filter(Q(date=date) | Q(date_done=date)).filter(user=request.user)

    data = serializers.serialize('json', list(tasks), indent=4, use_natural_foreign_keys=True,
                                 use_natural_primary_keys=True)

    return HttpResponse(data, content_type='application/json')


@csrf_exempt
@login_required()
def check_as_done(request):
    if request.POST:
        userTaskId = request.POST.get('userTaskId')
        resume = request.POST.get('resume')
        close_relative_tasks = request.POST.get('close_relative_tasks')

        user_task = get_object_or_404(UserTask, id=userTaskId)
        if user_task.resume:
            user_task.resume += '<hr/>'
        else:
            user_task.resume = ''
        user_task.resume += '<span class="date">' + datetime.today().strftime('%d %b %H:%M') + '</span> ' + resume
        user_task.resume += '<hr/><span class="date">' + datetime.today().strftime(
            '%d %b %H:%M') + '</span> <b>hecho</b>'

        user_task.done = True
        user_task.date_done = datetime.today()
        user_task.save()

        if close_relative_tasks:
            user_tasks = UserTask.objects.filter(user=user_task.user, content_type=user_task.content_type,
                                                 object_id=user_task.object_id, object_repr=user_task.object_repr
                                                 )
            for user_task in user_tasks:
                if not user_task.done:
                    if user_task.resume:
                        user_task.resume += '<hr/>'
                    else:
                        user_task.resume = ''
                    user_task.resume += '<span class="date">' + datetime.today().strftime(
                        '%d %b %H:%M') + '</span> Tarea cerrada por otra tarea; ' + resume
                    user_task.resume += '<hr/><span class="date">' + datetime.today().strftime(
                        '%d %b %H:%M') + '</span> <b>hecho</b>'
                    user_task.active = False
                    user_task.save()

    response = {
        'success': 'true'
    }

    return HttpResponse(json.dumps(response), content_type='application/json')


@csrf_exempt
@login_required()
def check_as_postpone(request):
    if request.POST:
        userTaskId = request.POST.get('userTaskId')
        resume = request.POST.get('resume')
        date = request.POST.get('date')

        userTask = get_object_or_404(UserTask, id=userTaskId)
        if userTask.resume:
            userTask.resume += '<hr/>'
        else:
            userTask.resume = ''
        userTask.resume += '<span class="date">' + datetime.today().strftime('%d %b %H:%M') + '</span> - ' + resume
        userTask.resume += '<hr/><span class="date">' + datetime.today().strftime(
            '%d %b %H:%M') + '</span> <b>pospuesto</b> del ' + userTask.date.strftime(
            '%d %b') + ' al ' + datetime.strptime(date, '%Y-%m-%d').strftime('%d %b')
        userTask.date_postpone = date
        userTask.done = False
        userTask.save()

    response = {
        'success': 'true'
    }

    return HttpResponse(json.dumps(response), content_type='application/json')


def new(request):
    if request.POST:
        form = UserTaskForm(request.POST)
        if form.is_valid():
            user_task = form.save(commit=False)
            user_task.user = request.user
            if user_task.done:
                user_task.date_done = user_task.date
            user_task.save()
            return redirect(reverse('diary:tasks_todo'))
    else:
        form = UserTaskForm()
    return render(request, 'main/base_form.html', locals())
